import express from "express";
import { connection, mongodb, UPDATE_OPERATORS, isMongoId } from "../func/db.js";
import { user_collection, getCurrentUserId, getMicroUser, getUserById, updateUser, getUserFavorites, getUserFavoritesCount, getUserUploads, getUserUploadsCount } from "./user.js";
import { authenticate, authenticateUser } from "../func/auth_helper.js";
import { parseYoutubeDuration } from "../func/utils.js";
import request from "request";
import * as wanakana from "wanakana";
import { i18n } from "../func/localization.js";

import { TrackModel } from "../models/track.js";
import { PlaylistModel } from "../models/playlist.js";

import * as Errors from "../models/errors.js";
import { CommentModel } from "../models/comment.js";
import { MicroUser } from "../models/user.js";

const router = express.Router();

export let track_collection;
export let featured_collection;
export let playlist_collection;

export const TRACK_COLLECTION_NAME = "song-links";

const FETCH_RETURN_SIZE = 42;

const SPECIAL_PLAYLIST_IDS = ["favorite", "own_uploads"];

initSongCollection();
function initSongCollection() {
    connection.then((db_connection) => {
        db_connection.collection(TRACK_COLLECTION_NAME, (err, result) => {
            if (err || result === undefined) {
                console.error(`${err}\nCould not find song links collection`);
                return err;
            }

            // Store the collection connection
            track_collection = result;
        });

        db_connection.collection("featured", (err, result) => {
            if (err || result === undefined) {
                console.error(`${err}\nCould not find featured collection`);
                return err;
            }

            // Store the collection connection
            featured_collection = result;
        });

        db_connection.collection("playlists", (err, result) => {
            if (err || result === undefined) {
                console.error(`${err}\nCould not find playlist collection`);
                return err;
            }

            // Store the collection connection
            playlist_collection = result;
        });
    }).catch(err => {
        console.error(err + "\nCould not get track data... Retrying in 5 seconds...");
        setTimeout(initSongCollection, 5000);
    });
}

router.get("/page", (req, res) => {
	try {
		let offset = 0;
		let size = FETCH_RETURN_SIZE;
		if (req.query.offset)
			offset = parseInt(req.query.offset);
		if (req.query.size)
			size = parseInt(req.query.size);

		authenticate(req, () => {
			getPagedTracks(offset, size, (err, result, total_size, result_size) => {
				result.forEach((val) => {
					val.search_keywords = [val.name].concat(wanakana.tokenize(wanakana.toRomaji(val.name)));
				});

				res.send({
					items: result,
					total_size: total_size,
					result_size
				});
			});
		}, () => {
			res.status(401).end();
		});
	} catch (err) {
		console.error(`${err}\nFailed to get all songs' info`);
		res.status(500).end();
	}
});

router.get("/all", (req, res) => {
    try {
        authenticate(req, () => {
            getAllTracks((err, result) => {
                result.forEach((val) => {
                    val.search_keywords = [val.name].concat(wanakana.tokenize(wanakana.toRomaji(val.name)));
                });

                res.send(result);
            });
        }, () => {
            res.status(401).end();
        });
    } catch (err) {
        console.error(`${err}\nFailed to get all songs' info`);
        res.status(500).end();
    }
});

router.get("/featured", (req, res) => {
    try {
        featured_collection.find({}).toArray((err, result) => {
            if (err) {
                console.error(err);
                res.status(500).send({
                    error: new Errors.ProcessingError("Could not get featured posts"),
                    data: null
                });
                return;
            }

            res.send({
                error: null,
                data: result[0]
            });
        });
    } catch (err) {
        console.error(err);
        res.status(500).send({
            error: new Errors.ProcessingError("Could not get featured posts"),
            data: null
        });
    }
});

async function _getPlaylistTracks(id, offset, size) {
	let data = await playlist_collection.aggregate([
		{
			$match: {
				_id: mongodb.ObjectId(id)
			}
		},
		{
			$addFields: {
				total_count: {
					$size: "$tracks",
				},
			}
		},
		{ // HACK: This complicated lookup is to ensure that we get the same array order as the original field
			$lookup: {
				from: TRACK_COLLECTION_NAME,
				let: {
					track_id: "$tracks"
				},
				pipeline: [{
					$match: {
						$expr: {
							$in: ["$_id", "$$track_id"]
						}
					},
				},
				{
					$addFields: {
						sort: {
							$indexOfArray: ["$$track_id", "$_id"]
						}
					}
				},
				{
					$sort: {
						sort: 1
					}
				},
				{
					$addFields: {
						sort: "$$REMOVE"
					}
				},
				{
					$skip: offset
				},

				{
					$limit: size
				},
				],
				localField: "tracks",
				foreignField: "_id",
				as: "tracks"
			}
		},
	]).toArray();
	return data;
}


router.get("/playlist/track-ids", (req, res) => {
	try {
		authenticate(req, async () => {
			if (isMongoId(req.query.id)) {
				const data = await _getPlaylist(req.query.id);
				if (!data) {
					res.send({
						"error": "Track not found",
						"data": null
					});
					return;
				}

				console.log("data");
				console.log(data);


				res.send({
					"error": null,
					"data": data.tracks
				});
			} else if (SPECIAL_PLAYLIST_IDS.includes(req.query.id)) {
				const user = await authenticateUser(req);

				let tracks;

				let user_info = await getUserById(getCurrentUserId(req));

				if (req.query.id == SPECIAL_PLAYLIST_IDS[0]) {
					tracks = user_info.favorites;
				} else {
					tracks = user_info.uploads;
				}

				res.send({
					"error": null,
					"data": tracks.reverse()
				});
			}








		}, () => {
			res.status(401).end();
		});
	} catch (err) {
		console.error(err);
		res.status(500).send({
			error: new Errors.ProcessingError(),
			data: null
		});
	}
});


router.get("/playlist/check-track", (req, res) => {
	try {
		authenticate(req, async () => {
			if (isMongoId(req.query.track_id) && isMongoId(req.query.playlist_id)) {
				const data = await _getPlaylist(req.query.playlist_id);
				let include = false;

				for (let i = 0; i < data.tracks.length; i++) {
					if (String(data.tracks[i]) === req.query.track_id) {
						include = true;
						break;
					}
				}

				res.send({
					"error": null,
					"data": include
				});
			}
		}, () => {
			res.status(401).end();
		});
	} catch (err) {
		console.error(err);
		res.status(500).send({
			error: new Errors.ProcessingError(),
			data: null
		});
	}
});



router.get("/playlist/detail", async (req, res) => {
	try {
		if (req.query.id) {
			const offset = parseInt(req.query.offset) || 0;
			const size = parseInt(req.query.size) || FETCH_RETURN_SIZE;

			if (isMongoId(req.query.id)) {
				const data = await _getPlaylistTracks(req.query.id, offset, size);

				res.send({
					error: null,
					data: data ? data[0] : null,
					is_end: offset + size > data[0].total_count,
				});
			} else if (SPECIAL_PLAYLIST_IDS.includes(req.query.id)) {
				const user = await authenticateUser(req);

				let data, tracks;

				if (req.query.id == SPECIAL_PLAYLIST_IDS[0]) {
					// Favorites
					data = {
						_id: req.query.id,
						name: req.__("playlist_favorite"),
						created_at: null,
						tracks: [],
						track_count: 0,
						is_special: true,
					};

					let track_data = await user_collection.aggregate([
						{
							$match: {
								_id: mongodb.ObjectId(user._id)
							}
						},
						{
							$addFields: {
								total_count: {
									$size: "$favorites",
								},
							}
						},
						{ // HACK: This complicated lookup is to ensure that we get the same array order as the original field
							$lookup: {
								from: TRACK_COLLECTION_NAME,
								let: {
									track_id: "$favorites"
								},
								pipeline: [{
									$match: {
										$expr: {
											$in: ["$_id", "$$track_id"]
										}
									},
								},
								{
									$addFields: {
										sort: {
											$indexOfArray: ["$$track_id", "$_id"]
										}
									}
								},
								{
									$sort: {
										sort: -1
									}
								},
								{
									$addFields: {
										sort: "$$REMOVE"
									}
								},
								{
									$skip: offset
								},

								{
									$limit: size
								},
								{
									$project: {
										comments: 0,
										favorites: 0
									}
								}
								],
								localField: "favorites",
								foreignField: "_id",
								as: "tracks"
							}
						},
					]).toArray();

					data.track_count = track_data[0].total_count;
					tracks = track_data[0].tracks;
				} else {
					// Uploads
					data = {
						_id: req.query.id,
						name: req.__("playlist_own_uploads"),
						created_at: null,
						tracks: [],
						track_count: user.uploads.count,
						is_special: true,
					};

					let track_data = await user_collection.aggregate([
						{
							$match: {
								_id: mongodb.ObjectId(user._id)
							}
						},
						{
							$addFields: {
								total_count: {
									$size: "$uploads",
								},
							}
						},
						{ // HACK: This complicated lookup is to ensure that we get the same array order as the original field
							$lookup: {
								from: TRACK_COLLECTION_NAME,
								let: {
									track_id: "$uploads"
								},
								pipeline: [{
									$match: {
										$expr: {
											$in: ["$_id", "$$track_id"]
										}
									},
								},
								{
									$addFields: {
										sort: {
											$indexOfArray: ["$$track_id", "$_id"]
										}
									}
								},
								{
									$sort: {
										sort: -1
									}
								},
								{
									$addFields: {
										sort: "$$REMOVE"
									}
								},
								{
									$skip: offset
								},

								{
									$limit: size
								},
								{
									$project: {
										comments: 0,
										favorites: 0
									}
								}
								],
								localField: "uploads",
								foreignField: "_id",
								as: "tracks"
							}
						},
					]).toArray();

					data.track_count = track_data[0].total_count;
					tracks = track_data[0].tracks;
				}

				data.tracks = tracks;

				res.send({
					error: null,
					data,
					is_end: offset + size >= data.track_count,
				});
			} else {
				res.status(400).end();
			}
		}
	} catch (err) {
		console.error(err);
		res.status(500).send({
			error: new Errors.ProcessingError("Could not get playlist tracks"),
			data: null
		});
	}
});


router.get("/playlist/related", async (req, res) => {
	try {
		if (req.query.track_id && isMongoId(req.query.track_id)) {
			let offset = parseInt(req.query.offset) || 0;
			let size = parseInt(req.query.size) || FETCH_RETURN_SIZE;


			let playlists = await playlist_collection.aggregate(
				[
					{
						$match: {
							tracks: mongodb.ObjectId(req.query.track_id)
						}
					},
					{ // HACK: This complicated lookup is to ensure that we get the same array order as the original field
						$lookup: {
							from: "user",
							localField: "user",
							pipeline: [
								{
									$project: {
										username: 1,
										avatar_url: 1,
									}
								}
							],

							foreignField: "_id",
							as: "user",
						}

					},
					{
						$sort: {
							created_at: -1
						}
					},
					{
						$skip: offset
					},

					{
						$limit: size
					},
				]
			).toArray();

			playlists.forEach(item => {
				item.user = item.user[0];
			});

			console.log("playlists retlade");
			console.log(playlists);

			res.send({
				error: null,
				data: playlists
			});
		} else {
			res.status(400).end();
		}
	} catch (err) {
		console.error(err);
		res.status(500).send({
			error: new Errors.ProcessingError("Could not get related playlist"),
			data: null
		});
	}
});


router.get("/playlist", async (req, res) => {
	try {
		if (req.query.id) {
			res.send({
				error: null,
				data: await _getPlaylist(req.query.id)
			});
		} else {
			// Get own playlists
			let user = await authenticateUser(req);

			if (user) {
				let playlists = await playlist_collection.find({
					user: mongodb.ObjectId(user._id)
				}).toArray();

				playlists.forEach(i => {
					i.is_special = false;
					i.track_count = i.tracks.length;
					i.tracks = [];
				});

				// Add special playlists
				playlists.unshift({
					_id: "favorite",
					name: req.__("playlist_favorite"),
					created_at: null,
					tracks: [],
					track_count: await getUserFavoritesCount(user._id),
					thumbnail_url: user.avatar_url,
					is_special: true,
				});
				playlists.unshift({
					_id: "own_uploads",
					name: req.__("playlist_own_uploads"),
					created_at: null,
					tracks: [],
					track_count: await getUserUploadsCount(user._id),
					thumbnail_url: user.avatar_url,
					is_special: true,
				});

				res.send({
					error: null,
					data: playlists
				});
			} else {
				res.status(401).send({
					error: new Errors.ValidationError("Unauthenticated request"),
					data: null
				});
			}
		}
	} catch (err) {
		console.error(err);
		res.status(500).send({
			error: new Errors.ProcessingError("Could not get playlist"),
			data: null
		});
	}
});

router.delete("/", async (req, res) => {
	let user = await authenticateUser(req);

	if (user) {
		if (!req.body.track_id) {
			res.send({
				error: new Errors.InvalidParameterError(),
				data: null
			});
			return;
		}

		try {
			res.send({
				error: null,
				data: await deleteTrack(req.body.track_id)
			});
		} catch (err) {
			res.status(500).send({
				error: new Errors.ProcessingError("Could not delete track"),
				data: null
			});
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});

router.post("/playlist", async (req, res) => {
	let user = await authenticateUser(req);

	if (user) {
		if (!req.body.tracks || !req.body.name || !Array.isArray(req.body.tracks) || req.body.name.length > 100) {
			res.send({
				error: new Errors.InvalidParameterError(),
				data: null
			});
			return;
		}

		try {
			res.send({
				error: null,
				data: await _createPlaylist(req.body.name.trim(), new Date(), user._id, req.body.tracks)
			});
		} catch (err) {
			res.status(500).send({
				error: new Errors.ProcessingError("Could not create playlist"),
				data: null
			});
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});


router.delete("/playlist", async (req, res) => {
	let user = await authenticateUser(req);

	if (user) {
		if (!req.body.id) {
			res.send({
				error: new Errors.InvalidParameterError(),
				data: null
			});
			return;
		}

		try {
			res.send({
				error: null,
				data: await deletePlaylist(req.body.id)
			});
		} catch (err) {
			res.status(500).send({
				error: new Errors.ProcessingError("Could not delete playlist"),
				data: null
			});
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});


router.post("/playlist/add", async (req, res) => {
	let user = await authenticateUser(req);

	if (user) {
		if (!req.body.track_id || !req.body.playlist_id) {
			res.send({
				error: new Errors.InvalidParameterError(),
				data: null
			});
			return;
		}

		try {
			res.send({
				error: null,
				data: await _addTrackToPlaylist(req.body.track_id, req.body.playlist_id)
			});
		} catch (err) {
			res.status(500).send({
				error: new Errors.ProcessingError("Could not add track to playlist"),
				data: null
			});
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});

router.post("/playlist/remove", async (req, res) => {
	let user = await authenticateUser(req);

	if (user) {
		if (!req.body.track_id || !req.body.playlist_id) {
			res.send({
				error: new Errors.InvalidParameterError(),
				data: null
			});
			return;
		}

		try {
			res.send({
				error: null,
				data: await _removeTrackFromPlaylist(req.body.track_id, req.body.playlist_id)
			});
		} catch (err) {
			res.status(500).send({
				error: new Errors.ProcessingError("Could not add track to playlist"),
				data: null
			});
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});

router.get("/info", (req, res) => {
	let track_id = req.query.id;
	if (!track_id) {
		res.status(400).end();
		return;
	}

	getTrackById(track_id).then((result) => {
		res.send({
			error: null,
			data: result
		});
	}).catch(() => {
		res.send({
			error: "Failed to get track with ID",
			data: null
		});
	});
});

router.get("/related", async (req, res) => {
	const data = await getRelatedTracks();
	res.send({
		error: null,
		data: data
	});
});

router.get("/random", async (req, res) => {
	const data = await getRandomTrack();
	res.send({
		error: null,
		data: data[0]
	});
});

router.post("/id", (req, res) => {
	getTrackById(req.body.id).then((result) => {
		res.send({
			error: null,
			data: result
		});
	}).catch(() => {
		res.send({
			error: "Failed to get track with ID",
			data: null
		});
	});
});

router.post("/:id/favorite", async (req, res) => {
	if (req.session.userInfo) {
		let result = await addTrackToFavorite(req.params.id, getCurrentUserId(req));
		res.send(result);
	} else {
		res.status(401).send(new Errors.ValidationError("Unauthenticated request"));
	}
});

router.post("/youtube-id", (req, res) => {
	_getYoutubeTrackById(req.body.id).then(data => {
		if (data.items[0].contentDetails.licensedContent) {
			// Video cannot be played on other sites
			res.send({
				error: new Errors.ValidationError(req.__("error_track_licensing")),
				data: null
			});
			return;
		}

		res.send({
			error: null,
			data: data
		});
	}).catch(err => {
		res.send({
			error: err,
			data: null
		});
	});
});

router.post("/upload", async (req, res) => {
	try {
		if (await authenticateUser(req)) {
			if (!req.body.name || !req.body.path || !req.body.img_src || !req.body.duration) {
				res.status(400).send({
					error: new Errors.ValidationError("Bad request")
				});
				return;
			}

			console.log("Received request to upload song info with name: " + req.body.name + " and path: " + req.body.path);

			let user_info = await getUserById(getCurrentUserId(req));
			if (user_info) {
				let result = await uploadTrack(new TrackModel(req.body.path, req.body.name, req.body.description ?? "", new Date(), new MicroUser(user_info._id.toHexString(), user_info.username, user_info.avatar_url), req.body.img_src, req.body.img_src_wide, parseYoutubeDuration(req.body.duration)), user_info._id.toHexString());
				res.send({
					error: result.error,
				});
			}
		} else {
			res.status(401).send({
				error: new Errors.ValidationError("Unauthenticated request")
			});
		}
	} catch (err) {
		console.error("Failed to upload track");
		res.status(500).send({
			error: new Errors.ProcessingError("Failed to upload track")
		});
	}
});

router.get("/comment", async (req, res) => {
	try {
		let result = await getAllPostsWithComments();
		res.send({
			error: null,
			data: result
		});
	} catch (err) {
		res.send({
			error: new Errors.ProcessingError("Failed to get all comments"),
			data: null
		});
	}
});

router.get("/comment-homepage", async (req, res) => {
	try {
		let result = await getHomepageComments();
		res.send({
			error: null,
			data: result
		});
	} catch (err) {
		res.send({
			error: new Errors.ProcessingError("Failed to get all comments"),
			data: null
		});
	}
});


router.post("/comment", async (req, res) => {
	if (await authenticateUser(req)) {
		try {
			res.send(await postComment(getCurrentUserId(req), req.body.content, req.body.post_id));
		} catch (err) {
			console.error(err);
			res.status(500).send({
				error: new Errors.ProcessingError("Could not post your comment"),
				data: null
			});
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});


router.put("/update", async (req, res) => {
	if (await authenticateUser(req)) {
		if (!req.body.id) {
			res.send({
				error: new Errors.InvalidParameterError(),
				data: null
			});
		} else {
			let track = getTrackById(req.body.id);

			if (!track) {
				res.send({
					error: new Errors.NotFoundError(),
					data: null
				});
			} else {
				if (req.body.name) {
					await updateTrack(req.body.id, "name", req.body.name.trim(), UPDATE_OPERATORS["SET"]);
				}

				if (req.body.img_src) {
					await updateTrack(req.body.id, "img_src", req.body.img_src.trim(), UPDATE_OPERATORS["SET"]);
				}

				if (req.body.img_src_wide) {
					await updateTrack(req.body.id, "img_src_wide", req.body.img_src_wide.trim(), UPDATE_OPERATORS["SET"]);
				}

				if (req.body.path) {
					// TODO: Get data of the new path with the youtube API


					await updateTrack(req.body.id, "path", req.body.path.trim(), UPDATE_OPERATORS["SET"]);
				}

				if (req.body.description !== null) {
					if (req.body.description.length > 2000) {
						res.send({
							error: new Errors.InvalidParameterError("Description exceeded length limit of 2000 characters"),
							data: null
						});
						return;
					}

					await updateTrack(req.body.id, "description", req.body.description.trim(), UPDATE_OPERATORS["SET"]);
				}

				let updated_track = getTrackById(req.body.id);

				res.send({
					error: null,
					data: updated_track
				});
			}
		}
	} else {
		res.status(401).send({
			error: new Errors.ValidationError("Unauthenticated request"),
			data: null
		});
	}
});


export async function postComment(user_id, content, post_id) {
	try {
		let comment = new CommentModel(new mongodb.ObjectId(), await getMicroUser(user_id), content, new Date());

		await track_collection.updateOne({ _id: mongodb.ObjectId(post_id) }, {
			$push: {
				comments: comment
			}
		});

		return {
			error: null,
			data: comment
		};
	} catch (err) {
		console.error(err, "Could not post user" + user_id + "'s comment to post ID: " + post_id);
		return {
			error: new Errors.ProcessingError("Could not post comment")
		};
	}
}


export function getHomepageComments() {
	return new Promise((resolve, reject) => {
		track_collection.find({
			comments: {
				$exists: true,
				$not: {
					$size: 0
				}
			}
		}).sort({
			upload_time: -1
		}).limit(10).toArray((err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}


export function getAllPostsWithComments() {
	return new Promise((resolve, reject) => {
		track_collection.find({
			comments: {
				$exists: true,
				$not: {
					$size: 0
				}
			}
		}).toArray((err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		});
	});
}

export async function getPagedTracks(offset = 0, size = 42, callback) {
	const total_size = await track_collection.countDocuments();

	track_collection.find({}).sort({
		upload_time: -1
	}).skip(offset).limit(size).toArray((err, result) => {
		callback(err, result, total_size, result.length);
	});
}

export function getAllTracks(callback) {
	track_collection.find({}).toArray((err, result) => {
		callback(err, result);
	});
}

export async function getRandomTrack(size = 1) {
	return await track_collection.aggregate([{ $sample: { size: size } }]).toArray();
}


export async function getRelatedTracks(size = 5) {
	return await getRandomTrack(size);
}


export function getTrackById(id) {
	return new Promise((resolve, reject) => {
		track_collection.findOne({ _id: mongodb.ObjectId(id) }).then(result => {
			resolve(result);
		}).catch(err => {
			console.error(err, "Could not get track with ID: " + id);
			reject(new Errors.ProcessingError("Could not get track by ID"), null);
		});
	});
}

export function uploadTrack(song_info, poster_id) {
	return new Promise((resolve) => {
		// Detect duplicates with name, for now...
		track_collection.findOne({ name: song_info.name }).then(result => {
			if (!result) {
				track_collection.insertOne(song_info).then(result => {
					console.log("Uploaded track with id" + song_info._id + " and name: " + song_info.name);

					updateUser(poster_id, "uploads", mongodb.ObjectId(result.insertedId.toHexString()), "$push");

					resolve({
						error: null
					});
				});
			} else {
				resolve({
					error: new Errors.TrackUploadedError("Track is already uploaded")
				});
			}
		});
	});
}

export function deletePlaylist(id) {
	return new Promise((resolve, reject) => {
		playlist_collection.findOne({ _id: mongodb.ObjectId(id) }).then(result => {
			if (!result) {
				resolve();
			} else {
				playlist_collection.deleteOne({ _id: mongodb.ObjectId(id) }).then(() => {
					resolve();
				}).catch(() => {
					reject();
				});
			}
		});
	});
}


export function deleteTrack(track_id) {
	return new Promise((resolve, reject) => {
		track_collection.findOne({ _id: mongodb.ObjectId(track_id) }).then(result => {
			if (!result) {
				resolve();
			} else {
				track_collection.deleteOne({ _id: mongodb.ObjectId(track_id) }).then(() => {
					playlist_collection.update({
						tracks: mongodb.ObjectId(track_id)
					}, {
						$pull: {
							tracks: mongodb.ObjectId(track_id)
						}
					}
					);

					user_collection.update({
						_id: mongodb.ObjectId(result.poster._id)
					}, {
						$pull: {
							uploads: mongodb.ObjectId(track_id)
						}
					}
					);

					resolve();
				}).catch(() => {
					reject();
				});
			}
		}).catch(() => {
			reject();
		});
	});
}


export function updateTrack(id, key, data, update_operator) {
	return new Promise((resolve, reject) => {
		track_collection.updateOne({ _id: mongodb.ObjectId(id) }, {
			[update_operator]: {
				[key]: data
			}
		}).then(() => {
			resolve();
		}).catch(err => {
			reject(new Errors.ProcessingError(err));
		});
	});
}

async function addTrackToFavorite(track_id, user_id) {
	try {
		const info = await getUserById(user_id);

		const track_oid = mongodb.ObjectId(track_id);

		const fav_str = info.favorites.map(i => i.toString());

		if (fav_str.includes(track_id)) {
			await updateUser(user_id, "favorites", track_oid, "$pull");
			await updateTrack(track_id, "favorites", user_id, "$pull");
			info.favorites.splice(fav_str.indexOf(track_id), 1);
		} else {
			await updateUser(user_id, "favorites", track_oid, "$addToSet");
			await updateTrack(track_id, "favorites", user_id, "$addToSet");
			info.favorites.push(track_oid);
		}

		return {
			error: null,
			data: info.favorites
		};
	} catch (err) {
		console.error(err.message, "Could not add track to favorite");
		return {
			error: err
		};
	}
}

async function _getPlaylist(id) {
	let playlist = await playlist_collection.findOne({ _id: mongodb.ObjectId(id) });

	if (!playlist) return null;

	let user = await getUserById(playlist.user, { username: 1, avatar_url: 1 });

	if (!user) return null;

	// let tracks = await track_collection.find({ _id: { $in: playlist.tracks } }).toArray();
	playlist.user = user;

	return playlist;
}

function _createPlaylist(name, created_at, user_id, tracks) {
	return new Promise((resolve, reject) => {
		let converted_tracks = [];
		tracks.forEach(element => {
			converted_tracks.push(mongodb.ObjectId(element));
		});

		playlist_collection.insertOne(
			new PlaylistModel(
				name,
				created_at,
				mongodb.ObjectId(user_id),
				converted_tracks
			)
		).then(result => {
			resolve(result.ops[0]);
		}).catch(() => {
			reject();
		});
	});
}

function _addTrackToPlaylist(track_id, playlist_id) {
	return new Promise(async (resolve, reject) => {
		let track = await getTrackById(track_id);

		if (!track) return;

		let playlist = await playlist_collection.findOne({ _id: mongodb.ObjectId(playlist_id) });

		if (!playlist) return;

		if (playlist.tracks.includes(track_id)) return;

		if (!playlist.thumbnail_url) {
			await playlist_collection.updateOne({ _id: mongodb.ObjectId(playlist_id) }, {
				"$push": {
					tracks: mongodb.ObjectId(track_id)
				},
				"$set": {
					thumbnail_url: track.img_src_wide || track.img_src
				}
			});
		} else {
			await playlist_collection.updateOne({ _id: mongodb.ObjectId(playlist_id) }, {
				"$push": {
					tracks: mongodb.ObjectId(track_id)
				},
			});
		}

		resolve();
	});
}

function _removeTrackFromPlaylist(track_id, playlist_id) {
	return new Promise(async (resolve, reject) => {
		let track = await getTrackById(track_id);

		if (!track) return;

		let playlist = await playlist_collection.findOne({ _id: mongodb.ObjectId(playlist_id) });

		if (!playlist) return;

		if (!playlist.tracks.includes(track_id)) resolve();

		await playlist_collection.updateOne({ _id: mongodb.ObjectId(playlist_id) }, {
			"$pull": {
				tracks: mongodb.ObjectId(track_id)
			},
		});

		playlist = await playlist_collection.findOne({ _id: mongodb.ObjectId(playlist_id) });

		let url = null;
		if (playlist.tracks[0]) {
			track = await getTrackById(playlist.tracks[0]);
			url = track.img_src_wide || track.img_src;
		}

		await playlist_collection.updateOne({ _id: mongodb.ObjectId(playlist_id) }, {
			"$set": {
				thumbnail_url: url
			}
		});

		resolve();
	});
}

function _getYoutubeTrackById(id) {
	return new Promise((resolve, reject) => {
		let url = `https://youtube.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=${id}&key=${process.env.YOUTUBE_API_KEY}`;

		request.get(url, {
			"Accept": "application/json"
		}, (err, res) => {
			if (err) {
				reject(`${err}\nFailde to get track name with ID`);
				return;
			}

			try {
				resolve(JSON.parse(res.body));
			} catch (err) {
				reject(`${err}\nerror when getting video name from YouTube Data API`);
			}
		});
	});
}

export { router as trackRoutes };
