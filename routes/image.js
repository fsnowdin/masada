import express from "express";
import { authenticate } from "../func/auth_helper.js";
import request from "request";
import { getRangedRandomIndex } from "../func/utils.js";

// Init route
export const imageRoutes = express.Router();

const BASE_BOORU_URL = process.env.BOORU_URL;

export const USE_SZURUBOORU_CONTENT = BASE_BOORU_URL !== "";

const EXCLUDED_TAGS = USE_SZURUBOORU_CONTENT ? JSON.parse(process.env.BOORU_EXCLUDED_TAGS) : null;

const DEFAULT_BG_INFO = {
    game_name: process.env.DEFAULT_BG_GAME_NAME,
    path: process.env.DEFAULT_BG_URL,
    artist_name: process.env.DEFAULT_BG_ARTIST_NAME,
    source: process.env.DEFAULT_BG_SOURCE
};

const CLIENT_TYPE = {
    mobile: 0,
    desktop: 1,
};

const ACCEPTABLE_RATIO = 1.33;
const MAX_BG_RETRY_COUNT = 10;

export const BG_INFO_404 = DEFAULT_BG_INFO;


const MAX_RETRY_COUNT = 5;
let retry_count = 0;

// Cache when initBG info is called so we don't have to repeat CDN calls
let all_bg_info = [];

// An ordered by games -> artist -> filename version of the bg data
let ordered_all_bg_info = {};

const tag_categorized_bg_info = {};

// Update BG info periodically
setInterval(() => {
    console.log("Updating BG info...");
    initBG();
}, process.env.UPDATE_INTERVAL);

initBG();

// Return CDN path to a random BG and info about game name and artist name
imageRoutes.get("/random", (req, res) => {
    try {
        authenticate(req, () => {
            let info;

            if (req.query.type === "limited") {
                info = getLimitedRandomBG(process.env.MAX_DYNAMIC_BG_SIZE, req.query.client_type);
            } else if (req.query.type === "unlimited") {
                info = getRandomBG();
            }

            if (!info) {
                console.error("BG unintialized");
                res.status(500).end();
            }

            res.send(info);
        }, () => {
            res.status(401).end();
        });
    } catch (err) {
        console.error(err, "Could not process random BG request");
        res.status(500).end();
    }
});


imageRoutes.get("/tags", (req, res) => {
	try {
		authenticate(req, () => {
			if (Object.keys(all_bg_info).length === 0 && USE_SZURUBOORU_CONTENT) {
				res.status(500).end();
			} else if (!USE_SZURUBOORU_CONTENT) {
				res.send({
					base_url: "",
					info: ordered_all_bg_info
				});
			} else {
				let tags = Object.keys(ordered_all_bg_info);

				res.send({
					"error": null,
					"data": tags
				});
			}
		}, () => {
			res.status(401).end();
		});
	} catch (err) {
		console.error(`${err}\n Failed to get BG tags`);
		res.status(500).end();
	}
});

imageRoutes.get("/tags/data", (req, res) => {
	try {
		authenticate(req, () => {
			if (Object.keys(all_bg_info).length === 0 && USE_SZURUBOORU_CONTENT) {
				res.status(500).end();
			} else if (!USE_SZURUBOORU_CONTENT) {
				res.send({
					base_url: "",
					info: ordered_all_bg_info
				});
			} else {
				let tag = req.query.tag;
				if (!tag) {
					res.status(400).end();
					return;
				}
				let size = parseInt(req.query.size) || 42;
				let offset = parseInt(req.query.offset) || 0;

				if (!Object.prototype.hasOwnProperty.call(tag_categorized_bg_info, tag)) {
					res.send({
						"error": "Tag not found",
						"data": null
					});
					return;
				}

				res.send({
					"error": null,
					"data": tag_categorized_bg_info[tag].slice(offset, offset + size),
					"is_end": offset + size >= tag_categorized_bg_info[tag].length,
				});
			}
		}, () => {
			res.status(401).end();
		});
	} catch (err) {
		console.error(`${err}\n Failed to get BG tags`);
		res.status(500).end();
	}
});


imageRoutes.get("/ordered-all", (req, res) => {
    try {
        authenticate(req, () => {
            if (Object.keys(all_bg_info).length === 0 && USE_SZURUBOORU_CONTENT) {
                res.status(500).end();
            } else if (!USE_SZURUBOORU_CONTENT) {
                res.send({
                    base_url: "",
                    info: ordered_all_bg_info
                });
            } else {
                res.send({
                    base_url: `${BASE_BOORU_URL}/`,
                    info: ordered_all_bg_info
                });
            }
        }, () => {
            res.status(401).end();
        });
    } catch (err) {
        console.error(`${err}\n Failed to get ordered background art data`);
        res.status(500).end();
    }
});

// Get the BG info from the Szurubooru instance
function initBG() {
    if (!USE_SZURUBOORU_CONTENT) {
        _processBGData();
        return;
    }

    let total_count;
    let total_posts = [];

    _getBooruPosts(0, (res) => {
        try {
            let data = JSON.parse(res.body);

            total_count = data.total;

            total_posts = total_posts.concat(data.results);

            let index = 100;

            if (process.env.NODE_ENV !== "test") {
                // Repeatedly call the API until we get all the posts on the booru
                _getBooruPosts(index, function processBooruApiRequest(res, offset) {
                    let data = JSON.parse(res.body);
                    total_posts = total_posts.concat(data.results);

                    if (total_posts.length >= total_count) {
                        _processBGData(total_posts);
                    } else {
                        // Delay a bit so we don't overload the server
                        setTimeout(() => { _getBooruPosts(offset + 100, processBooruApiRequest); }, 500);
                    }
                });
            } else {
                _processBGData(total_posts);
            }
        } catch (err) {
            console.error(err, "booru API failed to respond... retrying in 5 seconds...");
            retry_count++;
            if (retry_count === MAX_RETRY_COUNT) {
                console.log("Booru API retry count exceeded.");
                return;
            }
            setTimeout(initBG, 5000);
        }
    }, (err) => {
        console.error(err, "booru API failed to respond... retrying in 5 seconds...");
        retry_count++;
        if (retry_count === MAX_RETRY_COUNT) {
            console.log("Booru API retry count exceeded.");
            return;
        }
        setTimeout(initBG, 5000);
    });

    function _getBooruPosts(offset, callback, fail_callback) {
        request.get(BASE_BOORU_URL + "/api/posts/?offset=" + offset, {
            "auth": {
                "user": process.env.BOORU_USERNAME,
                "pass": process.env.BOORU_PASSWD
            },
            "headers": {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        }, (err, res) => {
            if (err || res === undefined) {
                console.error(err);
                fail_callback(err);
                return;
            }

            callback(res, offset);
        }
        );
    }
}

export function getRandomBG() {
    if (Object.keys(all_bg_info).length === 0 && USE_SZURUBOORU_CONTENT) {
        console.error("BG info unintialized");
        return {};
    } else if (!USE_SZURUBOORU_CONTENT) {
        return DEFAULT_BG_INFO;
    }

    const bg = all_bg_info[getRangedRandomIndex(all_bg_info.length)];

    return {
        game_name: bg.game_name,
        artist_name: bg.artist_name,
        path: `${BASE_BOORU_URL}/${bg.url}`,
        source: `${BASE_BOORU_URL}/post/${bg.id}`
    };
}

export function getLimitedRandomBG(size = process.env.MAX_DYNAMIC_BG_SIZE, type = "desktop") {
    if (Object.keys(all_bg_info).length === 0 && USE_SZURUBOORU_CONTENT) {
        console.error("BG info unintialized");
        return {};
    } else if (!USE_SZURUBOORU_CONTENT) {
        return DEFAULT_BG_INFO;
    }

    const small_bgs = all_bg_info.filter(i => i.fileSize < size);

    let bg;

    let i = 0;
    while (i < MAX_BG_RETRY_COUNT) {
        bg = small_bgs[getRangedRandomIndex(small_bgs.length)];

        const ratio = bg.canvasWidth / bg.canvasHeight;

        if (CLIENT_TYPE[type] === CLIENT_TYPE["desktop"]) {
            if (ratio >= ACCEPTABLE_RATIO) {
                break;
            }
        } else {
            if (ratio < ACCEPTABLE_RATIO) {
                break;
            }
        }
        i++;
    }

    if (!bg) return DEFAULT_BG_INFO;

    return {
        game_name: bg.game_name,
        artist_name: bg.artist_name,
        path: `${BASE_BOORU_URL}/${bg.url}`,
        source: `${BASE_BOORU_URL}/post/${bg.id}`
    };
}

export function getWallpaper() {
    return DEFAULT_BG_INFO;
}

function _processBGData(post_data) {
    if (USE_SZURUBOORU_CONTENT) {
        all_bg_info = [];
        ordered_all_bg_info = {};

        for (let i = 0; i < post_data.length; i++) {
            let current_post = post_data[i];

            if (current_post.safety === "safe" && (current_post.type === "image" || current_post.type === "animation")) {
                // Skip if post has excluded tags
                if (current_post.tags.find(tag => EXCLUDED_TAGS.includes(tag.names[0]))) continue;

                let info = current_post;
                info.url = current_post.contentUrl;
				info.fullContentURL = new URL(BASE_BOORU_URL + "/" + current_post.contentUrl).href;
				info.fullThumbnailURL = new URL(BASE_BOORU_URL + "/" + current_post.thumbnailUrl).href;

                for (let j = 0; j < current_post.tags.length; j++) {
                    if (current_post.tags[j].category === "game") {
                        info.game_name = current_post.tags[j].names[0];
                        break;
                    } else if (current_post.tags[j].category === "artist") {
                        info.artist_name = current_post.tags[j].names[0];
                    } else {
                        if (info.game_name && info.artist_name) break;
                    }
                }

                if (info.game_name === undefined || info.artist_name === undefined) continue;

                all_bg_info.push(info);

                if (!ordered_all_bg_info[info.game_name]) ordered_all_bg_info[info.game_name] = {};
                if (!ordered_all_bg_info[info.game_name][info.artist_name]) ordered_all_bg_info[info.game_name][info.artist_name] = [];

                ordered_all_bg_info[info.game_name][info.artist_name].push(info);

				if (!tag_categorized_bg_info[info.game_name]) tag_categorized_bg_info[info.game_name] = [];
				tag_categorized_bg_info[info.game_name].push(info);
            }
        }

        console.log("BG info initialized successfully");
    } else {
        const bg_info = {};
        bg_info[DEFAULT_BG_INFO.game_name] = {};
        bg_info[DEFAULT_BG_INFO.game_name][DEFAULT_BG_INFO.artist_name] = [DEFAULT_BG_INFO];
        return bg_info;
    }
}
