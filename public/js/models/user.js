export class UserModel {
    _id;
    username;
    password;
    email;
    avatar_url;
    rank;
    uploads=[];
    registered_on;
    description;
    preferences={};
    custom_background_url;
    favorites=[];

    constructor(id, username, password, email, avatar_url, rank, registered_on) {
        this._id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.avatar_url = avatar_url;
        this.rank = rank;
        this.registered_on = registered_on;
    }
}

export class MicroUser {
    _id;
    username;
    avatar_url;

    constructor(id, username, avatar_url) {
        this._id = id;
        this.username = username;
        this.avatar_url = avatar_url;
    }
}