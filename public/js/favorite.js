/* global profile_info */

import * as HttpHandler from "./modules/http.js";
import trackItemComponent from "./components/track_item.js";
import { getAllSongsInfo } from "./modules/song.js";

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

getProfileInfo();
function getProfileInfo() {
    let username = profile_info.username;

    HttpHandler.getAsync(HttpHandler.GET_USER_INFO_URL + username).then((res) => {
        if (res && res.status === 200) {
            let data = JSON.parse(res.responseText);

            if (data.error) {
                showToastMessage("Something went wrong...");
            } else {
                Vue.createApp({
                    components: {
                        "track-item": trackItemComponent
                    },
                    data() {
                        return {
                            favorites_list: [],
                            is_empty : false,
                            profile_info_binding: null
                        };
                    },
                    mounted() {
                        this.profile_info_binding = data.info;

                        // get uploads data
                        getAllSongsInfo().then(data => {
                            this.all_songs_info = data;
                            this.profile_info_binding.favorites.forEach(val => {
                                let result = this.all_songs_info.find(i => i._id === val);
                                if (result) this.favorites_list.push(result);
                            });
                            this.favorites_list.reverse();
                            if (this.favorites_list.length === 0) this.is_empty = true;
                        }).catch(() => {
                            showToastMessage("Something went wrong...");
                        });
                    },
                }).mount("#app");
            }
        }
    }).catch(() => {
        setTimeout(() => {
            getProfileInfo();
        }, 3000);
    });
}

function showToastMessage(message) {
    toastList[0].show();
    toastElList[0].querySelector(".toast-body > p").innerText = message;
}
