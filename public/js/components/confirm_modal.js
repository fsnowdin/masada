const confirmModalComponent = {
    props: ["title", "body", "accept", "reject"],
    template: `
        <div class="modal" tabindex="-1" role="dialog" ref="modal" id="confirm-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ title }}</h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"> </button>
                    </div>

                    <div class="modal-body" v-show="body">
                        <p>{{ body }}</p>
                    </div>

                    <div class="p-4 d-flex justify-content-end w-100">
                        <button type="button" @click="reject" class="round-button px-4 py-1" data-bs-dismiss="modal">{{ $t('message.confirm_modal_no') }}</button>
                        <button type="button" @click="accept" data-bs-dismiss="modal" class="round-button px-4 py-1 ms-3">{{ $t('message.confirm_modal_yes')}}</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            title: null,
            body: null,
            accept: null,
            reject: null,
        };
    },
    methods: {
        show(title, body, accept, reject) {
            this.title = title;
            this.body = body;
            this.accept = accept;
            this.reject = reject;
        }
    }
};

export default confirmModalComponent;
