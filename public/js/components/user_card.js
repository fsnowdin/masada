const userCardComponent = {
    props: ["title", "href", "img_src", "desc", "desc2", "tooltip"],
    template: `
        <div class=" col-12 col-sm-4 col-xxl-3 user-item pb-3 pb-sm-0" v-bind:title="tooltip" style="height: 8rem;">
            <div class="user-item-container h-100 w-100 card-background">
                <a v-bind:href="href" class="h-100 w-100" >
                    <div class="d-flex flex-row align-items-center w-100 h-100">
                        <img class="img-fluid h-100 w-25" v-bind:src="img_src" loading="lazy" />

                        <div class="d-flex flex-column w-75 h-100 px-3">
                            <p class="pt-2 mw-100 user-item-title"> {{ title }} </p>
                            <p class="mw-100 user-item-description text-tertiary" style="white-space: nowrap;"> {{ desc }} </p>
                            <div ref="desc" class="mw-100 user-item-description overflow-hidden" ></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    `,
    mounted() {
        if (DOMPurify && marked) {
            this.$refs.desc.innerHTML = DOMPurify.sanitize(marked.parseInline(this.desc2));
        }
    }
};

export default userCardComponent;
