export const commentItemComponent = {
    props: ["username", "href", "avatar_url", "content", "timestamp"],
    template: `
        <div class="comment-item">
            <div class="comment-item-container flex-row d-flex h-100 w-100">
                <a :href="href">
                    <img class="user-avatar rounded-circle img-fluid flex-shrink-0" style="height: 3rem; width: 3rem;" :src="avatar_url" loading="lazy" />
                </a>

                <div class="d-flex flex-column w-75 h-100 px-3">
                    <div class="w-100 d-flex flex-column flex-sm-row flex-wrap">
                        <a :href="href" class="text-decoration-none fw-bold me-2">
                            <p class="comment-username"> {{ username }} </p>
                        </a>

                        <p class="text-tertiary comment-timestamp" :title="timestamp"> {{ getRelativeTime }} </p>
                    </div>
                    <div class="mw-100 comment-content text-secondary" v-html="getContent"></div>
                </div>
            </div>
        </div>
    `,
    computed: {
        getContent() {
            if (DOMPurify && marked) {
                return DOMPurify.sanitize(marked.parse(this.content));
            }
        },
        getRelativeTime() {
            if (moment) {
                return moment(this.timestamp).fromNow();
            }

            return this.timestamp;
        }
    }
};

export const microCommentItemComponent = {
    props: ["username", "href", "avatar_url", "content", "timestamp"],
    template: `
        <div class="comment-item ">
            <div class="comment-item-container flex-row d-flex h-100 w-100 ">
                <a :href="href">
                    <img class="user-avatar rounded-circle img-fluid flex-shrink-0" style="height: 3rem; width: 3rem;" :src="avatar_url" loading="lazy" />
                </a>

                <div class="d-flex flex-column w-75 h-100 px-3">
                    <div class="w-100 d-flex flex-column flex-sm-row flex-wrap">
                        <a :href="href" class="text-decoration-none fw-bold me-2">
                            <p class="comment-username"> {{ username }} </p>
                        </a>

                        <p class="text-tertiary comment-timestamp" :title="timestamp"> {{ getRelativeTime }} </p>
                    </div>
                    <div class="w-100 h-75 comment-content text-secondary overflow-hidden" style="text-overflow: ellipsis;display: -webkit-box; -webkit-line-clamp:2; -webkit-box-orient:vertical;" v-html="getContent"></div>
                </div>
            </div>
        </div>
    `,
    computed: {
        getContent() {
            if (DOMPurify && marked) {
                return DOMPurify.sanitize(marked.parseInline(this.content));
            }
        },
        getRelativeTime() {
            if (moment) {
                return moment(this.timestamp).fromNow();
            }

            return this.timestamp;
        }
    }
};
