/* global user_info, use_szurubooru_content */

"use strict";

import * as HttpHandler from "./modules/http.js";
import { addToFavorite, getAllSongsInfo } from "./modules/song.js";

import trackItemComponent from "./components/track_item.js";
import { commentItemComponent } from "./components/comment_item.js";
import progressBarComponent from "./components/progress_bar.js";
import confirmModalComponent from "./components/confirm_modal.js";

import { LOADING_ICON_PATH } from "./modules/utils.js";

import { addComment } from "./modules/comment.js";
import { TrackModel, PlaylistModel } from "./models/track.js";

import { UserModel } from "./models/user.js";

import { i18n } from "./modules/locale.js";

import { getRangedRandomIndex } from "./modules/utils.js";

// Connect to Socket.IO server
const socket = io();

const HOTKEYS_SCOPES = {
    INPUT: "input",
    PLAYER: "player",
    BG_PICKER: "bg_picker"
};

let search_initialized = false;
let autocom;

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

let is_mobile = window.matchMedia("only screen and (max-width: 760px)").matches; // Simple check for mobile
if (!is_mobile) particlesJS.load("particles-js", "/assets/particles.json", () => { });

let video_initialized = false;

let app = Vue.createApp({
    components: {
        "comment-item": commentItemComponent,
        "track-item": trackItemComponent,
        "progress-bar": progressBarComponent,
        "confirm-modal": confirmModalComponent
    },
    data() {
        return {
            user_info_binding: new UserModel(),
            user_playlists: [],
            has_fetched_playlist: false,

            create_playlist_info: new PlaylistModel(),
            playlist_detail_info: new PlaylistModel(),
			playlist_detail_tracks: [],
            is_adding_to_playlist: false,
			is_playlist_by_current_user: false,

            all_bg_info: {},
            bg_picker_tabs: [],
            bg_picker_images_list: [],
			bg_picker_is_end: false,
			bg_picker_offset: false,
            bg_picker_current_tab: this.$t("message.bg_picker_header"),
            bg_picker_detail_info: {
                url: LOADING_ICON_PATH,
                game_name: "",
                artist_name: ""
            },
            current_bg_index: 1,
            is_bg_change_locked: false,
            bg1_src: "/assets/icons/bg2_hack.png",
            bg2_src: "/assets/icons/bg2_hack.png",
            bg_game_name: "",
            bg_artist_name: "",
            next_bg_info: {},

            time_counter_text: "00:00",

            listener_count: 1,
            anonymous_count: 0,

            popular_song_info: {
                info: {
                    name: ""
                },
                listeners: []
            },

            is_looping: false,
            loop_count: 0,
            is_playing: false,
            progress_slider_value: 50,

            is_chat_detail_collapsed: false,
            is_logged_into_chat: false,
            chat_room_users_list: [],
            has_notification: false,
            user_song_update_task: null,
            adding_to_favorite_task: false,
            messages_list: [],
            chat_input_text: "",

            is_player_hidden: false,

            play_stack: [],
            play_stack_index: 0,

            is_in_playlist: false,
            is_playlist_loop_mode: false,
			playlist_offset: 0,
			playlist_is_end: false,
			current_opened_playlist: null,

            // all_songs_info: {},
			cached_songs_info: {},
            current_song_info: new TrackModel("", "", "", "", new UserModel("id", "poster"), ""),
            is_editing_song: false,
            edit_data: {},

            player: {},

            stored_volume_value: 50, // For when muting
            volume_value: 50,
            is_volume_slider_visible: false,

            is_all_songs_repeating: false, // Whether the user has gone through every single song

            is_shortcuts_blocked: false,

            comment_input: "",

			related_playlists: [],
			is_related_playlists_empty: false,

            related_tracks: [],
            is_related_tracks_empty: false
        };
    },
    computed: {
        isCurrentTrackFavorited() {
            if (!this.current_song_info || !this.user_info_binding) return false;
            return this.user_info_binding.favorites.includes(this.current_song_info._id);
        },
        getCurrentTrackUploadDate() {
            return moment(this.current_song_info.upload_time).fromNow();
        }
    },
    methods: {
        showPlaylist() {
            this.is_adding_to_playlist = false;
            this.togglePlaylist();
        },

        playPlaylist(playlist) {
            this.play_stack = [];
            this.play_stack_index = -1;
            this.is_in_playlist = true;

			playlist.tracks.forEach(track => {
				this.play_stack.push(track._id);
			});

            this.playNext();
        },

        async togglePlaylist() {
            let result = await HttpHandler.getUserPlaylists();
            if (result.error) {
                showToastMessage(this.$t("message.toast_failed_to_get_playlist"));
            } else {
                this.has_fetched_playlist = true;
                this.user_playlists = result.data;
				this.refreshPlaylistAddableState();
            }
        },

        async isTrackInPlaylist(track, playlist) {
			const data = await HttpHandler.getIsTrackInPlaylist(track._id, playlist._id);

			return data.data;
        },

        async startAddToPlaylist() {
			this.$refs.progressbar.setStartedStatus();
			this.user_playlists = [];
            let result = await HttpHandler.getUserPlaylists();
            if (result.error) {
                showToastMessage(this.$t("message.toast_failed_to_get_playlist"));
            } else {
                this.user_playlists = result.data;
                this.has_fetched_playlist = true;
                this.is_adding_to_playlist = true;
				this.refreshPlaylistAddableState();
            }
			this.$refs.progressbar.setFinishedStatus();
        },

		refreshPlaylistAddableState() {
			this.user_playlists.forEach(async item => {
				if (!item.is_special) {
					item.can_add = !(await this.isTrackInPlaylist(this.current_song_info, item));
				}
			});
		},

        async addTrackToPlaylist(track_info, playlist_info) {
			this.$refs.progressbar.setStartedStatus();
            let result = await HttpHandler.addTrackToPlaylist(track_info._id, playlist_info._id);
            if (result.error) {
                showToastMessage(this.$t("message.toast_failed_to_add_to_playlist"));
            } else {
                this.togglePlaylist();
				this.refreshPlaylistAddableState();
            }
			this.$refs.progressbar.setFinishedStatus();
        },

        async removeCurrentTrackFromPlaylist(track_info, playlist_info) {
			this.$refs.progressbar.setStartedStatus();
            let result = await HttpHandler.removeTrackFromPlaylist(track_info._id, playlist_info._id);
            if (result.error) {
                showToastMessage(this.$t("message.toast_failed_to_remove_from_playlist"));
            } else {
                this.togglePlaylist();
            }
			this.$refs.progressbar.setFinishedStatus();
        },

        async removeTrackFromPlaylist(track_info, playlist_info) {
			this.$refs.progressbar.setStartedStatus();
            let result = await HttpHandler.removeTrackFromPlaylist(track_info._id, playlist_info._id);
            if (result.error) {
                showToastMessage(this.$t("message.toast_failed_to_remove_from_playlist"));
            } else {
				this.openPlaylistDetail(playlist_info._id);
				this.startAddToPlaylist();
				this.refreshPlaylistAddableState();
            }
			this.$refs.progressbar.setFinishedStatus();
        },

        async openPlaylistDetail(id) {
			this.$refs.progressbar.setStartedStatus();

			this.playlist_detail_tracks = [];
			this.playlist_offset = 0;
			this.playlist_is_end = false;
			this.current_opened_playlist = id;
			const data = await HttpHandler.getPlaylistDetail(id);

			this.playlist_offset = this.playlist_offset + HttpHandler.FETCH_SIZE;
			this.playlist_is_end = data.is_end;

            this.playlist_detail_info = data.data;

			this.is_playlist_by_current_user = this.user_info_binding._id ?  this.playlist_detail_info.user === this.user_info_binding._id : false;
			this.playlist_detail_tracks = this.playlist_detail_info.tracks;

			this.$refs.progressbar.setFinishedStatus();
        },

		async playlistLoadMore() {
			if (!this.current_opened_playlist) return;

			this.$refs.progressbar.setStartedStatus();

			const data = await HttpHandler.getPlaylistDetail(this.current_opened_playlist, this.playlist_offset);

			this.playlist_offset = this.playlist_offset + HttpHandler.FETCH_SIZE;

			this.playlist_is_end = data.is_end;

			data.data.tracks.forEach((item) => {
				this.playlist_detail_tracks.push(item);
			});

			this.$refs.progressbar.setFinishedStatus();
		},

        async createPlaylist() {
            let result = await HttpHandler.createPlaylist(this.create_playlist_info.name);
            if (result.error) {
                showToastMessage(this.$t("message.toast_failed_to_create_playlist"));
            } else {
                this.user_playlists.push(result.data);
                this.create_playlist_info.name = "";
            }
        },

        joinChatRoom() {
            if (!user_info) {
                showToastMessage(this.$t("message.toast_chat_must_be_logged_in"));
            } else {
                socket.emit("join", this.user_info_binding);
                this.is_logged_into_chat = true;
                this.updateUserCurrentSong(this.current_song_info);
            }
        },
        leaveChatRoom() {
            socket.emit("leave");
            this.is_logged_into_chat = false;
        },
        playPopularSong() {
            this.playSong(this.popular_song_info._id);
            this.updateRelatedTracks();
			this.updateRelatedPlaylists();
        },
        togglePlay() {
            if (this.is_playing) {
                this.player.pauseVideo();
            } else {
                this.player.playVideo();
            }

            this.is_playing = !this.is_playing;
        },
        togglePlayerVisibility() {
            this.is_player_hidden = !this.is_player_hidden;
        },
        toggleFullscreen() {
            if (!document.fullscreenElement) {
                document.documentElement.requestFullscreen();
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                }
            }
        },
        toggleVideoLoop() {
			if (!this.is_looping && !this.is_playlist_loop_mode) {
				this.is_looping = true;
			} else {
				if (!this.is_playlist_loop_mode) {
					this.is_looping = false;
					this.is_playlist_loop_mode = true;
				} else {
					this.is_looping = false;
					this.is_playlist_loop_mode = false;
				}
			}
        },
        toggleMute() {
            if (this.volume_value > 0) {
                this.stored_volume_value = this.volume_value;
                this.volume_value = 0;
            } else {
                this.volume_value = this.stored_volume_value;
            }

            this.player.setVolume(Math.floor(this.volume_value));
        },
        sendMessage(type, sender_data, event, data) {
            if (this.is_logged_into_chat) {
                this.chat_input_text = "";

                socket.emit("message", {
                    type: type,
                    user: sender_data,
                    event: event,
                    data: data
                });
            } else {
                showToastMessage(this.$t("message.toast_chat_must_be_logged_in"));
            }
        },
        updateURL(id, name) {
            if (id) {
                window.history.replaceState({
                    track_id: id,
                    name: name
                }, "", `?track_id=${id}`);

                // Update the page title
                if (name) {
                    document.title = name;
                }
            }
        },

        async findSongInfoById(id) {
			if (!Object.prototype.hasOwnProperty.call(this.cached_songs_info, id)) {
				const data = await HttpHandler.getTrackById(id);
				this.cached_songs_info[id] = data["data"];
			}

			return {
				index: id,
				info: this.cached_songs_info[id]
			};
        },

        updatePlayerTime() {
            let duration = this.player.getDuration();
            let percentage = this.progress_slider_value / 100;

            let new_time = duration * percentage;

            this.player.seekTo(new_time);
        },

        changeVolume() {
            this.player.setVolume(Math.floor(this.volume_value));
        },

        copyCurrentTrackURL() {
            let url = `${window.location.protocol}//${window.location.hostname}/?track_id=${this.current_song_info._id}`;
            this.$refs["clipboard"].value = url;
            this.$refs["clipboard"].select();
            this.$refs["clipboard"].setSelectionRange(0, 999999); // For mobile devices

            document.execCommand("copy");

            // Clear the focus so the mobile keyboard won't show up
            document.activeElement.blur();

            showToastMessage(this.$t("message.toast_track_link_copied"));
        },

        preloadBG() {
			if(!video_initialized) return;

            this.is_bg_change_locked = true;

            HttpHandler.getRandomBG(true, _is_screen_mobile_sized()).then(data => {
                // HACK: Wait for the BG to load, the lazy ass way
                setTimeout(() => {
                    this.is_bg_change_locked = false;
                }, 5000);

                if (this.is_bg_change_cancelled) {
                    this.is_bg_change_cancelled = false;
                    return;
                }

                this.next_bg_info = data;

                if (this.current_bg_index === 1) {
                    this.bg2_src = this.next_bg_info.path;
                } else {
                    this.bg1_src = this.next_bg_info.path;
                }
            });
        },

        enterBG() {
            this.bg_game_name = this.next_bg_info.game_name;
            this.bg_artist_name = this.next_bg_info.artist_name;
        },

        showNextBG() {
            if (this.is_bg_change_locked) {
                showToastMessage(this.$t("message.toast_new_bg_loading"));
                return;
            }

            this.is_bg_change_locked = true;

            if (this.current_bg_index === 2) {
                this.current_bg_index = 1;
            } else {
                this.current_bg_index = 2;
            }

            showToastMessage(this.$t("message.toast_bg_changed"));
        },

        async playPrevious() {
            if (this.play_stack.length > 1 && this.play_stack_index > 0) {
                this.play_stack_index--;

				this.current_song_info = (await this.findSongInfoById(this.play_stack[this.play_stack_index])).info;
                this.current_song_info.listener_count = 1;

                this.player.loadVideoById(this.current_song_info.path, 0);

                this.loop_count = 0;

                this.updateURL(this.current_song_info._id, this.current_song_info.name);

                this.updateUserCurrentSong(this.current_song_info);
                this.updateRelatedTracks();
				this.updateRelatedPlaylists();
            } else {
                showToastMessage("Already at the first song in the playing order...");
            }
        },

        turnOffPlaylistMode() {
            this.is_in_playlist = false;
            this.is_playlist_loop_mode = false;
        },

        askDeletePlaylist(playlist) {
            this.showConfirmModal(this.$t("message.playlist_delete_title", { name: playlist.name }), this.$t("message.playlist_delete_body"), async () => {
                let result = await HttpHandler.deletePlaylist(playlist._id);
                if (result.error) {
                    showToastMessage(this.$t("message.playlist_delete_fail"));
                } else {
                    showToastMessage(this.$t("message.playlist_delete_success"));
                }
            }, () => { });
        },

        async playNext(autoplay = true) {
            // See if we're at the top of the stack
            if (this.play_stack_index === this.play_stack.length - 1) {
                if (this.is_playlist_loop_mode) {
                    // Loop playlist
                    this.play_stack_index = 0;
					this.current_song_info = (await this.findSongInfoById(this.play_stack[0])).info;
                } else {
                    // Pick a new random song to play

					this.$refs.progressbar.setStartedStatus();
                    let data = await HttpHandler.getRandomTrack();
					let info = data["data"];

                    // Ensure a different song
                    while (this.play_stack.indexOf(info._id) !== -1 && !this.is_all_songs_repeating) {
						data = await HttpHandler.getRandomTrack();
						info = data["data"];
                    }

                    this.current_song_info = info;
					this.cached_songs_info[info._id] = info;

                    this.play_stack.push(info._id);

                    this.play_stack_index++;

					this.$refs.progressbar.setFinishedStatus();

                    // Turn on repeat if at the end of the playlist
                    // if (this.play_stack_index === this.all_songs_info.length - 1) {
                    //     this.is_all_songs_repeating = true;
                    // }
                }
            } else {
                this.play_stack_index++;
				this.current_song_info = (await this.findSongInfoById(this.play_stack[this.play_stack_index])).info;
            }

            this.current_song_info.listener_count = 1;

            if (autoplay) {
                this.player.loadVideoById(this.current_song_info.path, 0);
            } else {
                this.player.cueVideoById(this.current_song_info.path, 0);
            }

            this.loop_count = 0;

            this.updateUserCurrentSong(this.current_song_info);

            this.updateURL(this.current_song_info._id, this.current_song_info.name);

            this.updateRelatedTracks();
			this.updateRelatedPlaylists();
        },

        updateUserCurrentSong(info) {
            if (this.user_song_update_task) clearTimeout(this.user_song_update_task);

            this.user_song_update_task = setTimeout(() => {
                if (!this.current_song_info._id) return;

                if (!this.is_logged_into_chat) {
                    socket.emit("track-update", {
                        listening_to: this.current_song_info,
                        user: {},
                        message: null,
                    });
                } else {
                    socket.emit("track-update", {
                        listening_to: this.current_song_info,
                        user: this.user_info_binding,
                        message: `${this.user_info_binding.username} is now listening to ${info.name}`,
                    });
                }
            }, 1000);

            this.changeEditInfo(info);
        },

        async playSong(id) {
            if (id === undefined || id === "") {
                return;
            } else if (id === this.current_song_info._id) {
                showToastMessage(this.$t("message.toast_track_already_playing"));
                return;
            }

			this.$refs.progressbar.setStartedStatus();
            let result = await this.findSongInfoById(id);
            if (!result) return;

            this.current_song_info = result.info;
            this.current_song_info.listener_count = 1;

            // See if we're at the top of the stack
            if (this.play_stack_index === this.play_stack.length - 1) {
                this.play_stack_index++;
                this.play_stack.push(result.index);
            } else {
                // Insert the song ID into the play stack
                // [1, 2, 3] , we at index 1 and want to add the new song between 2 and 3
                // -> [1, 2, new, 3`] index is 2
                this.play_stack_index++;
                this.play_stack.splice(this.play_stack_index, 0, result.index);
            }

            this.player.loadVideoById(this.current_song_info.path, 0);

            this.loop_count = 0;

            this.updateUserCurrentSong(this.current_song_info);

            this.updateURL(this.current_song_info._id, this.current_song_info.name);

			this.$refs.progressbar.setFinishedStatus();

			this.updateRelatedPlaylists();
        },

        async playSongInPlaylist(track, playlist) {
            if (track === undefined || track === "") {
                return;
            } else if (track._id === this.current_song_info._id) {
                showToastMessage(this.$t("message.toast_track_already_playing"));
                return;
            }

            let result = await this.findSongInfoById(track._id);
            if (!result) return;

            this.current_song_info = result.info;
            this.current_song_info.listener_count = 1;

            // Find the index of the track in the playlist

			// Get full playlist track IDs
			playlist = (await HttpHandler.getPlaylistTrackIds(playlist._id)).data;

            const index = playlist.indexOf(track._id);

            this.play_stack = [];
            this.play_stack_index = index - 1;
            this.is_in_playlist = true;

            playlist.forEach(track => {
				this.play_stack.push(track);
            });

            this.playNext();
        },

		async updateRelatedPlaylists() {
			const data = await HttpHandler.getRelatedPlaylists(this.current_song_info._id);
			if (data && !data["error"]) {
				this.related_playlists = [];

				for (let i = 0; i < data["data"].length; i++) {
					const info = data["data"][i];
					this.related_playlists.push(info);
				}

				this.is_related_playlists_empty = this.related_playlists.length === 0;
			}
		},

        async updateRelatedTracks() {
			const data = await HttpHandler.getRelatedTracks();
            if (data && !data["error"]) {
                this.related_tracks = [];

                for (let i = 0; i < data["data"].length; i++) {
					const info = data["data"][i];
                    if (info._id === this.current_song_info._id) {
                        continue;
                    }
                    this.related_tracks.push(info);
                }

                this.is_related_tracks_empty = this.related_tracks.length < 1;
            }
        },

		async openRelatedPlaylist(id) {
			await this.openPlaylistDetail(id);
		},

		async playRelatedPlaylist(id) {
			// Get full playlist track IDs
			let playlist = (await HttpHandler.getPlaylistTrackIds(id)).data;

			this.play_stack = [];
			this.play_stack_index = -1;
			this.is_in_playlist = true;

			playlist.forEach(track => {
				this.play_stack.push(track);
			});

			this.playNext();
		},

        async playRelatedTrack(index) {
            this.playSong(this.related_tracks[index]._id);

			const data = await HttpHandler.getRandomTrack();

            this.related_tracks[index] = data["data"];
			this.updateRelatedPlaylists();
        },

        startEditTrack() {
            this.is_editing_song = !this.is_editing_song;

            this.changeEditInfo(this.current_song_info);
        },

        changeEditInfo(song_data) {
            this.edit_data.id = song_data._id;
            this.edit_data.name = song_data.name;
            this.edit_data.description = song_data.description;
            this.edit_data.img_src = song_data.img_src;
            this.edit_data.img_src_wide = song_data.img_src_wide;
            this.edit_data.path = song_data.path;
        },

        finishEditTrack() {
            HttpHandler.updateTrack(this.edit_data, (data) => {
                if (data.error) {
                    showToastMessage(this.$t("message.toast_track_updated_fail"));
                } else {
                    showToastMessage(this.$t("message.toast_track_updated"));
                    this.current_song_info.name = this.edit_data.name;
                    this.current_song_info.description = this.edit_data.description;
                    this.current_song_info.path = this.edit_data.path;
                    this.current_song_info.img_src = this.edit_data.img_src;
                    this.current_song_info.img_src_wide = this.edit_data.img_src_wide;

                    this.is_editing_song = false;
                }
            });
        },

        askDeleteTrack(song_info) {
            this.showConfirmModal(this.$t("message.edit_delete_track_modal_title", {track: song_info.name}), this.$t("message.edit_delete_track_modal_body"), async () => {
                let result = await HttpHandler.deleteTrack(song_info._id);
                if (result.error) {
                    showToastMessage(this.$t("message.toast_track_deleted_fail"));
                } else {
                    showToastMessage(this.$t("message.toast_track_deleted"));

                    this.play_stack.splice(this.play_stack_index, 1);
                    this.play_stack_index -= 1;
                    this.is_editing_song = false;

                    this.playNext(false);
                }
            }, () => { });
        },

        showConfirmModal(title, body, accept_callback, reject_callback) {
            this.$refs.confirm.show(title, body, accept_callback, reject_callback);
        },

        addMessageToChatWindow(type, sender, event, data) {
            let is_collapsed = false;
            const last_message = this.messages_list[0];
            if (type !== "system" && this.messages_list.length > 0 && last_message.type !== "system" && last_message.user) {
                is_collapsed = last_message.user.username === sender.username;
            }

            let text = "";
            // Event check
            switch (event) {
                case "user_join":
                    text = this.$t("message.chat_user_join_event", { user: data });
                    break;
                case "user_leave":
                    text = this.$t("message.chat_user_leave_event", { user: data });
                    break;
                case "track_update":
                    text = this.$t("message.chat_track_update_event", { user: data.user.username, track: data.track.name });
                    break;
                case "chat_current_user_leave_event":
                    text = this.$t("message.chat_current_user_leave_event");
                    break;
                case "chat_track_repeat_event":
                    text = this.$t("message.chat_track_repeat_event", { user: data.user.username, track: data.track.name, count: data.count });
                    break;
				case "chat_none_joined_event":
					text = this.$t("message.chat_none_joined_event");
					break;
                case "chat_send_message":
                    text = data.message;
                    break;
            }

            this.messages_list.unshift({
                type: type,
                user: sender,
                text: text,
                is_collapsed: is_collapsed
            });
        },

        sendWelcomeChatMessage() {
            if (this.chat_room_users_list.length === 0) {
                this.addMessageToChatWindow("system", "", "chat_none_joined_event");
            } else {
                // this.addMessageToChatWindow("system", "", "chat_some_joined_event");
            }
        },

        iframeAPIReadySetup() {
            this.player = new YT.Player("video-player", {
                videoId: this.current_song_info.path,
                playerVars: {
                    "playsinline": 1,
                    "autoplay": 1
                },
                events: {
                    // Start the app when the player is ready
                    "onReady": () => {
                        app.$refs.progressbar.setFinishedStatus();

						app.updateUserCurrentSong(app.current_song_info);

						app.play_stack.push(app.current_song_info._id);
						app.updateURL(app.current_song_info._id, app.current_song_info.name);

                        // Initialize the slider's value so it matches the player's volume
                        this.volume_value = this.player.getVolume();

                        // Update the time counter
                        setInterval(() => {
                            let duration = this.player.getDuration();
                            let current_time = this.player.getCurrentTime();

                            let percentage = (current_time / duration) * 100;
                            this.progress_slider_value = percentage;

                            let time;

                            if (duration === undefined || current_time === undefined) {
                                time = "0:00 / 0:00";
                            } else {
                                time = `${Math.floor(current_time / 60)}:${String(Math.floor(current_time % 60)).padStart(2, "0")} / ${Math.floor(duration / 60)}:${String(Math.floor(duration % 60)).padStart(2, "0")}`;
                            }

                            this.time_counter_text = time;
                        }, 10);


						// Init secondary features

						if (user_info) {
							HttpHandler.getUserInfoByUsername(user_info.username).then(data => {
								if (data.error) {
									showToastMessage(this.$t("message.toast_user_info_get_fail"));
									return;
								}

								this.user_info_binding = data.info;

								this.joinChatRoom();
							});
						} else if (!user_info || false) {
							// TODO: Check if user prefers not to automatically join c fshat rooms

							this.sendWelcomeChatMessage();
						}


						initSearchBox();

						HttpHandler.getRandomBG(true, _is_screen_mobile_sized()).then(data => {
							this.bg1_src = data.path;
							this.bg_game_name = data.game_name;
							this.bg_artist_name = data.artist_name;

							this.preloadBG();
						});

						app.updateRelatedTracks();
						app.updateRelatedPlaylists();


						socket.on("update-user-list", (list) => {
							this.chat_room_users_list = [];

							list.forEach(val => {
								this.chat_room_users_list.push(val);
							});
						});

						socket.on("update-listener-count", (data) => {
							this.listener_count = data.all;
							this.anonymous_count = data.anonymous;
						});

						socket.on("update-popular-songs", (data) => {
							this.popular_song_info = data;
						});

						// Start listening for new messages
						socket.on("message", (info) => {
							this.addMessageToChatWindow(info.type, info.user, info.event, info.data);

							// Notification
							if (document.hidden && info.type === "user" && this.is_logged_into_chat && info.username !== "" && this.user_info_binding.username !== info.user.username && !this.has_notification) {
								this.has_notification = true;
								document.title = `(*) ${this.current_song_info.name}`;
							}

							// this.$refs["chat-message-window"].scrollTop = this.$refs["chat-message-window"].scrollHeight;
						});

						socket.on("leave-success", () => {
							this.is_logged_into_chat = false;
							this.addMessageToChatWindow("system", null, "chat_current_user_leave_event");

						});

						socket.on("leave-fail", () => {
							showToastMessage(this.$t("message.toast_chat_room_leave_fail"));
							this.is_logged_into_chat = true;

						});

						// Handle keyboard shortcuts
						hotkeys.filter = (event) => {
							if (hotkeys.getScope() !== HOTKEYS_SCOPES.INPUT) {
								let tagName = (event.target || event.srcElement).tagName;
								return !(tagName.isContentEditable || tagName == "INPUT" || tagName == "SELECT" || tagName == "TEXTAREA");
							}

							return event.target.id === "comment_input";
						};

						hotkeys("ctrl+enter", HOTKEYS_SCOPES.INPUT, () => {
							this.comment();
						});
						hotkeys("h", HOTKEYS_SCOPES.PLAYER, () => {
							this.togglePlayerVisibility();
						});
						hotkeys("left", HOTKEYS_SCOPES.PLAYER, () => {
							this.playPrevious();
						});
						hotkeys("right", HOTKEYS_SCOPES.PLAYER, () => {
							this.playNext();
						});
						hotkeys("down", HOTKEYS_SCOPES.PLAYER, () => {
							this.togglePlay();
						});
						hotkeys("up", HOTKEYS_SCOPES.PLAYER, () => {
							this.togglePlayerVisibility();
						});
						hotkeys("space", HOTKEYS_SCOPES.PLAYER, () => {
							event.preventDefault();
							// Clear the focus so buttons don't get accidentally pressed
							document.activeElement.blur();
							this.togglePlay();
						});
						hotkeys("f", HOTKEYS_SCOPES.PLAYER, () => {
							this.toggleFullscreen();
						});
						hotkeys("l", HOTKEYS_SCOPES.PLAYER, () => {
							this.playNext();
						});
						hotkeys("j", HOTKEYS_SCOPES.PLAYER, () => {
							this.playPrevious();
						});
						hotkeys("k", HOTKEYS_SCOPES.PLAYER, () => {
							this.togglePlay();
						});
						hotkeys("r", HOTKEYS_SCOPES.PLAYER, () => {
							this.toggleVideoLoop();
						});
						hotkeys("m", HOTKEYS_SCOPES.PLAYER, () => {
							this.toggleMute();
						});
						hotkeys("g", HOTKEYS_SCOPES.PLAYER, () => {
							if (use_szurubooru_content) {
								this.showNextBG();
							}
						});
						hotkeys("c", HOTKEYS_SCOPES.PLAYER, () => {
							this.copyCurrentTrackURL();
						});
						hotkeys("s", HOTKEYS_SCOPES.PLAYER, () => {
							event.preventDefault(); // HACK: So the search box won't register the 's'

							if (this.is_player_hidden) this.togglePlayerVisibility();

							setTimeout(() => {
								// HACK: wait 50ms so the focus won't break when the player needs to be shown from a hidden state
								this.$refs["search-box"].focus();
							}, 50);
						});
						hotkeys("p", HOTKEYS_SCOPES.PLAYER, () => {
							if (use_szurubooru_content) {
								if (!this.is_player_hidden) this.togglePlayerVisibility();

								setTimeout(() => {
									this.$refs["pick-bg-button"].click();
								}, 50);
							}
						});

						hotkeys.setScope(HOTKEYS_SCOPES.PLAYER);

						video_initialized = true;
					},
					"onStateChange": event => {
						if (event.data == YT.PlayerState.ENDED) {
							if (this.is_looping) {
								this.player.seekTo(0, true);

								this.loop_count = this.loop_count + 1;

								if (this.loop_count % 5 === 0 && this.is_logged_into_chat) {
									this.sendMessage("system", this.user_info_binding, "chat_track_repeat_event", { user: this.user_info_binding, track: this.current_song_info, count: this.loop_count });
								}
							} else {
								this.playNext();
							}
						} else if (event.data === YT.PlayerState.PLAYING) {
							// Update the volume because on mobile the volume may reset on song change
							this.player.setVolume(this.volume_value);

							this.is_playing = true;
						} else if (event.data === YT.PlayerState.PAUSED) {
							this.is_playing = false;
						}
					}
				}
			});
		},

		initAllBGInfo() {
			this.populateBGPickerTabs();
		},

		async populateBGPickerTabs() {
			let game_names = (await HttpHandler.getBGTags()).data;

			this.bg_picker_tabs = game_names;

			this.openBGPickerTab(game_names[0]);
		},

		openBGPickerTab(game_name) {
			this.bg_picker_images_list = [];
			this.bg_picker_offset = 0;
			this.bg_picker_current_tab = game_name;
			this.populateBGPickerGrid(game_name);
		},

		async populateBGPickerGrid(game_name) {
			const data = await HttpHandler.getBGArt(game_name, this.bg_picker_offset);

			data["data"].forEach(item => {
				this.bg_picker_images_list.push({
					game_name: item.game_name,
					artist_name: item.artist_name,
					thumbnail_url: item.thumbnailUrl,
					content_url: item.url,
					art_type: item.type,
					full_thumbnail: item.fullThumbnailURL,
					full_content: item.fullContentURL,
				});
			});

			this.bg_picker_is_end = data["is_end"];

			this.bg_picker_offset = this.bg_picker_offset + HttpHandler.FETCH_SIZE;
		},

		openBGDetailModal(url, game_name, artist_name) {
			this.bg_picker_detail_info = {
				url: url,
				game_name: game_name,
				artist_name: artist_name
			};
		},

		exitBGDetailModal() {
			this.bg_picker_detail_info = {
				url: LOADING_ICON_PATH,
				game_name: "",
				artist_name: ""
			};
		},

		showBG(url, game_name, artist_name) {
			if (this.current_bg_index === 1) {
				this.bg2_src = url;
			} else {
				this.bg1_src = url;
			}

			this.next_bg_info = {
				url: url,
				game_name: game_name,
				artist_name: artist_name
			};

			if (this.is_bg_change_locked) {
				this.is_bg_change_cancelled = true;
			}

			this.showNextBG();
		},

		blockShortcuts() {
			this.is_shortcuts_blocked = true;
		},

		unblockShortcuts() {
			this.is_shortcuts_blocked = false;
		},

		async addTrackToFavorites(track_info) {
			if (!user_info) {
				showToastMessage(this.$t("message.toast_favorite_request_login"));
			} else {
				if (this.adding_to_favorite_task) return;

				this.$refs.progressbar.setStartedStatus();

				this.adding_to_favorite_task = true;

				addToFavorite(track_info, res => {
					if (res.error) {
						showToastMessage(this.$t("message.toast_favorite_fail"));
					} else {
						this.user_info_binding.favorites = res.data;
						let index = this.current_song_info.favorites.indexOf(this.user_info_binding._id);
						if (index !== -1) {
							this.current_song_info.favorites.splice(index, 1);
						} else {
							this.current_song_info.favorites.push(this.user_info_binding._id);
						}
					}

					this.adding_to_favorite_task = false;

					this.$refs.progressbar.setFinishedStatus();
				});
			}
		},

		async comment() {
			this.comment_input = this.comment_input.trim();
			if (this.comment_input === "") {
				showToastMessage(this.$t("message.toast_comment_invalid_text"));
				return;
			}

			this.$refs.progressbar.setStartedStatus();

			let result = await addComment(this.comment_input, this.current_song_info._id);

			if (result.error) {
				showToastMessage(this.$t("message.toast_comment_invalid_text"));
			} else {
				this.current_song_info.comments.push(result.data);
				this.comment_input = "";
			}

			this.$refs.progressbar.setFinishedStatus();
		},
		commentInputFocus() {
			hotkeys.setScope(HOTKEYS_SCOPES.INPUT);
		},
		commentInputBlur() {
			hotkeys.setScope(HOTKEYS_SCOPES.PLAYER);
		},
	},

	mounted() {
		{
			// Uboa lmao
			const logo = this.$refs["nav-logo"].querySelector("img");
			const og = logo.src;
			const chance = 64;
			let uboa_task;
			let was_rotating = false;

			this.$refs["nav-logo"].addEventListener("click", (event) => {
				event.preventDefault();

				if (uboa_task) return;

				was_rotating = logo.classList.contains("rotate");

				if (Math.floor(Math.random() * chance) === 1) {
					logo.classList.remove("rotate");
					logo.src = "/assets/icons/uboa.webp";

					uboa_task = setTimeout(() => {
						if (was_rotating) logo.classList.add("rotate");
						logo.src = og;
						clearTimeout(uboa_task);
						uboa_task = null;
						window.location.assign("/");
					}, 500);
				} else {
					window.location.assign("/");
				}
			});
		}

		i18n.global.locale = HttpHandler.setLangFromCookie();
	}
});

app.config.globalProperties.window = window;
app.config.globalProperties.document = document;
app.config.globalProperties.navigator = navigator;

app.use(i18n);

app = app.mount("#app");

{
	// Get the YouTube API
	let tag = document.createElement("script");
	tag.src = "https://www.youtube.com/iframe_api";
	let firstScriptTag = document.getElementsByTagName("script")[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

// Called when the embed player is ready to play
// HACK: Add onto window because we're using ES6 modules
window.onYouTubeIframeAPIReady = async () => {
	let urlParams = new URLSearchParams(window.location.search);
	let track_id = urlParams.get("track_id");
	let playlist_id = urlParams.get("playlist_id");

	app.$refs.progressbar.setStartedStatus();

	let result = await app.findSongInfoById(track_id);
	if (result) {
		app.current_song_info = result.info;

		// Turn on repeat if at the end of the playlist
		// if (app.play_stack_index === app.all_songs_info.length - 1) {
		// 	app.is_all_songs_repeating = true;
		// }

		// Check for playlists
		if (playlist_id) {
			HttpHandler.getPlaylist(playlist_id).then(res => {
				if (res.error) {
					showToastMessage(this.$t("message.toast_failed_to_get_playlist"));
					return;
				}

				// Add to the play stack
				res.data.posts.forEach(async (val) => {
					if (val === app.current_song_info._id) return;

					let result = await app.findSongInfoById(val);
					if (result) app.play_stack.push(result.index);
				});
			});
		}

		app.iframeAPIReadySetup();
	} else {
		showToastMessage("Something went wrong. Please refresh the page.");
	}
};

document.addEventListener("visibilitychange", () => {
	if (!document.hidden) {
		if (app.has_notification) {
			app.has_notification = false;
			document.title = app.current_song_info.name;
		}
	}
});

function showToastMessage(message) {
	toastList[0].show();
	toastElList[0].querySelector(".toast-body > p").innerText = message;
}

function initSearchBox() {
	autocom = new autoComplete({
		data: {
			src: null,
			keys: ["search_keywords"],
			cache: true,
		},
		selector: "#search-box",
		resultItem: {
			element: (item, data) => {
				item.innerHTML = data.value.name;
				item.innerText = data.value.name;
			},
		},
		resultsList: {
			maxResults: 20,
		},
	});

	document.getElementById("search-box").addEventListener("click", (event) => {
		if (!search_initialized) {
			getAllSongsInfo().then(result => {
				autocom.data.src = result;
				search_initialized = true;

				for (let i = 0; i < result.length; i++) {
					app.cached_songs_info[result[i]._id] = result[i];
				}
			});
		}
	});

	document.getElementById("search-box").addEventListener("selection", (event) => {
		document.getElementById("search-box").value = "";
		app.playSong(event.detail.selection.value._id);
	});
}

function _is_screen_mobile_sized() {
	return window.innerWidth < 760;
}
