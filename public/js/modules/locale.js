const messages = {
    en: {
        message: {
            listener_count: "{count} listening",

            homepage_next: "Next",
            homepage_previous: "Previous",

            chat_placeholder: "Say something",
            chat_leave_button: "Leave",
            chat_join_button: "Join",
            chat_users_header: "Chat room users",
            chat_send_button_tooltip: "Send message",

            chat_user_join_event: "{user} just joined",
            chat_user_leave_event: "{user} left",
            chat_current_user_leave_event: "You have left the chat room",
            chat_none_joined_event: "No one is here",
            chat_some_joined_event: "No one has joined the chat room yet",
            chat_track_update_event: "{user} is now listening to {track}",
            chat_track_repeat_event: "{user} have repeated {track} {count} times",

            playlist_header: "Playlists",
            playlist_create_placeholder: "Playlist name",
            playlist_add_button: "Add track to playlist",
            playlist_remove_button: "Remove track from playlist",
            playlist_play_button: "Play",
            playlist_delete_button: "Delete playlist",
            playlist_delete_title: "Delete {name} ",
            playlist_delete_body: "Really delete the playlist?",
            playlist_delete_fail: "Could not delete playlist...",
            playlist_delete_success: "Playlist deleted",

            comment_button: "Comment",
            comment_box_placeholder: "Say something about the track",
            comment_counter: "{count} Comments",

			related_playlists: "Related playlists",

            related_tracks: "Tracks you may like",
            favorite_button_tooltip: "Favorite",
            share_button_tooltip: "Copy the link to the track",
            edit_button_tooltip: "Edit",
            playlist_button_tooltip: "Add track to playlist",
            track_poster_post_time: "Posted {time}",
            popular_track_info: "{name} is popular right now with {count} listening",
            popular_track_play_tooltip: "Play {name}",
            art_credit: "{game} art by {artist}",
            bg_picker_set_as_bg: "Set as background",
            bg_picker_download: "Download",
            bg_picker_header : "Art picker",

            pick_bg: "Pick background art",
            random_bg: "Change to random background",

            loading: "Working... Please wait...",
            upload_invalid_url: "Invalid URL",
            upload_invalid_data: "Invalid upload data",
            upload_success: "Track uploaded",
            upload_fail: "Could not upload track",

            user_registered_on: "Registered: {time}",

            toast_failed_to_get_playlist: "Failed to get playlist",
            toast_failed_to_create_playlist: "Failed to create playlist",
            toast_failed_to_add_to_playlist: "Failed to add track to playlist",
            toast_failed_to_remove_from_playlist: "Failed to remove track from playlist",

            toast_chat_must_be_logged_in: "Log in to chat and more!",
            toast_chat_room_leave_fail: "Could not leave the chat room",
            toast_track_link_copied: "Copied the track's link to clipboard",
            toast_new_bg_loading: "Your new background is loading",
            toast_bg_changed: "Background changed",

            toast_track_already_playing: "You are already playing that track",
            toast_track_updated: "Track updated",
            toast_track_updated_fail: "Failed to update track",
            toast_track_deleted: "Track deleted",
            toast_track_deleted_fail: "Failed to delete track",

            toast_favorite_request_login: "Login or create an account to favorite the track",
            toast_favorite_fail: "Failed to favorite the track",

            toast_something_wrong: "Something went wrong...",

            toast_comment_invalid_text: "Invalid body for comment",
            toast_comment_fail: "Failed to send your comment",

            toast_user_info_get_fail: "Failed to get user info",

            toast_preferences_save: "Settings saved",
            toast_preferences_save_fail: "Failed to save settings",

            cancel: "Cancel",
            save: "Save",

            edit_name_header: "Name",
            edit_description_header: "Description",
            edit_thumbnail_header: "Thumbnail",
            edit_wide_thumbnail_header: "Wide Thumbnail",
            edit_path_header: "Path",
            edit_delete_track: "Delete Track",
            edit_delete_track_modal_title: "Delete {track}?",
            edit_delete_track_modal_body: "This will also delete all of the track's comments.",
            confirm_modal_no: "No",
            confirm_modal_yes: "Yes",

            login_in_progress: "Logging in...",
            login_fail: "Failed to log you in...",
            login_success: "Successfully logged in! Redirecting you to the homepage...",

            register_success: "You have been successfully registered",

            profile_avatar_uploading: "Uploading your new avatar...",
            profile_avatar_filesize: "",

            profile_background_uploading: "Uploading your new background...",
            profile_background_filesize: "",
            profile_background_remove_fail: "Failed to remove your custom background...",
            profile_background_remove: "Your custom background has been removed",

            profile_password_update: "Password updated successfully",
            profile_password_update_fail: "Failed to change your password",

            profile_description_update: "Failed to update your description",
            profile_description_update_fail: "Description updated successfully",
            footer_loop_count_tooltip: "You have looped the track {count} times",
        }
    },
    ja: {
        message: {
            listener_count: "{count}人が聴いています",

            homepage_next: "次",
            homepage_previous: "前",

            chat_placeholder: "何かを言って",
            chat_leave_button: "脱退",
            chat_join_button: "チャットルームを参加",
            chat_users_header: "チャットルームのユーザ",
            chat_send_button_tooltip: "メーセジを送る",

            chat_user_join_event: "{user}さんが来ました！",
            chat_user_leave_event: "{user}さんが脱退しました・・・",
			chat_current_user_leave_event: "あなたが脱退しました・・・",
            chat_none_joined_event: "誰もいません",
            chat_some_joined_event: "まだ誰もがチャットルームを参加しません",
            chat_track_update_event: "{user}さんが「{track}」を聴いています",
            chat_track_repeat_event: "{userさん}が「{track}」を{count}回聴きました",

            playlist_header: "再生リスト",
            playlist_create_placeholder: "再生リスト名",
            playlist_add_button: "再生リストにトラックを追加",
            playlist_remove_button: "再生リストにトラックを削除",
            playlist_play_button: "再生",
            playlist_delete_button: "再生リストを削除",
            playlist_delete_title: "{name}を削除",
            playlist_delete_body: "本当に削除しますか。",
            playlist_delete_fail: "再生リストを削除できません・・・",
            playlist_delete_success: "再生リストを削除しました",

            comment_button: "コメント",
            comment_box_placeholder: "このトラックはどう思いますか。",
            comment_counter: "{count}件のコメント",

			related_playlists: "関連の再生リスト",

            related_tracks: "他のトラック",
            favorite_button_tooltip: "いいね！",
            share_button_tooltip: "リンクをコーピ",
            edit_button_tooltip: "編集",
            playlist_button_tooltip: "再生リスとにトラックを追加",
            track_poster_post_time: "{time}に投稿",
            popular_track_info: "{count}人が「{name}」を聴いています",
            popular_track_play_tooltip: "クリックして「{name}」を再生します",
            art_credit: "{artist}さんに{game}の絵が描かれます",
            bg_picker_set_as_bg: "壁紙に設定",
            bg_picker_download: "ダウンロード",
            bg_picker_header : "絵のリスト",

            pick_bg: "壁紙を変更",
            random_bg: "ランダムの壁紙に変更",

            loading: "読み込んでいます・・・少々お待ちください・・・",
            upload_invalid_url: "リンクを利用できませんでした",
            upload_invalid_data: "アップロードデータを利用できませんでした",
            upload_success: "トラックを投稿しました",
            upload_fail: "トラックを投稿できませんでした",

            user_registered_on: "作成: {time}",

            toast_failed_to_get_playlist: "再生リストがありませんかもしれません",
            toast_failed_to_create_playlist: "再生リストを作成できません",
            toast_failed_to_add_to_playlist: "再生リストにトラックを追加できません",
            toast_failed_to_remove_from_playlist: "再生リストからトラックを削除できません",

            toast_chat_must_be_logged_in: "チャットのためにログインしてください",
            toast_chat_room_leave_fail: "チャットルームを去れません",
            toast_track_link_copied: "トラックのリンクをコーピしました",
            toast_new_bg_loading: "壁紙はローディングしています",
            toast_bg_changed: "壁紙を変更しました",

            toast_track_already_playing: "このトラックがもう再生しています",
            toast_track_updated: "トラックを変更しました",
            toast_track_updated_fail: "トラックを変更できません",
            toast_track_deleted: "トラックを削除しました",
            toast_track_deleted_fail: "トラックを削除できません",

            toast_favorite_request_login: "「いいね」のためにログインしてまたはアカウントを作成してください",
            toast_favorite_fail: "「いいね」できません",

            toast_something_wrong: "しまった・・・",

            toast_comment_invalid_text: "本文がありません",
            toast_comment_fail: "コメントできません",

            toast_user_info_get_fail: "ユーザデータを取れません",

            toast_preferences_save: "設定を保存しました",
            toast_preferences_save_fail: "設定を保存できません",

            cancel: "キャンセル",
            save: "保存",

            edit_name_header: "名",
            edit_description_header: "説明",
            edit_thumbnail_header: "サムネイル",
            edit_wide_thumbnail_header: "おおきいサムネイル",
            edit_path_header: "ID",

            edit_delete_track: "削除",
            edit_delete_track_modal_title: "{track}を削除しますか？",
            edit_delete_track_modal_body: "トラックのコメントも削除します。",

            confirm_modal_no: "いいえ",
            confirm_modal_yes: "はい",

            login_in_progress: "ログイン中・・・",
            login_fail: "ログインできません・・・",
            login_success: "ログインしました",

            register_success: "アカウントを作成しました",

            profile_avatar_uploading: "アバターをアップロドしています",
            profile_avatar_filesize: "",

            profile_background_uploading: "壁紙をアップロドしています",
            profile_background_filesize: "",
            profile_background_remove_fail: "壁紙を元に戻れません",
            profile_background_remove: "壁紙を元に戻りました",

            profile_password_update: "パスワードを変わりました",
            profile_password_update_fail: "パスワードを変われません・・・",

            profile_description_update: "自己紹介を変わりました",
            profile_description_updat_fail: "自己紹介を変われません・・・",

            footer_loop_count_tooltip: "トラックが{count}回聴かれました",
        }

    }
};

export const i18n = VueI18n.createI18n({
    locale: "ja",
    fallbackLocale: "en",
    messages
});
