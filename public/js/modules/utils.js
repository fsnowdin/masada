import * as HttpHandler from "./http.js";

export const LOADING_ICON_PATH = "/assets/icons/loading.gif";

export function getYoutubeThumbnailPath(youtube_id) {
    return `https://img.youtube.com/vi/${youtube_id}/hqdefault.jpg`;
}

export function getYoutubeTrackById(id, callback) {
	HttpHandler.postAsync(HttpHandler.GET_YOUTUBE_TRACK_BY_ID_URL, {
		id: id
	}, (response) => {
		response.json().then((data) => {
			callback(data);
		});
	});
}

export function getRangedRandomIndex(range) {
	return Math.floor(Math.random() * range);
}

export function formatBytes(bytes, decimals = 0) {
    if (bytes === 0) return "0 Bytes";

    const k = 1000;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}
