/* global MAX_AVATAR_SIZE */

import * as HttpHandler from "./modules/http.js";
import trackItemComponent from "./components/track_item.js";
import { i18n } from "./modules/locale.js";

import { getAllSongsInfo } from "./modules/song.js";
import { formatBytes } from "./modules/utils.js";

// Init Bootstrap Toast
let toastElList = [].slice.call(document.querySelectorAll(".toast"));
let toastList = toastElList.map(toastEl => {
    return new bootstrap.Toast(toastEl, {
        animation: false,
        delay: 2000
    });
});

// Passed by the server
let profile_info;

getProfileInfo();

function getMarkdownHTML(text) {
    return DOMPurify.sanitize(marked.parse(text));
}

function getProfileInfo() {
    let username = window.location.pathname.split("/")[2];

    HttpHandler.getAsync(HttpHandler.GET_USER_INFO_URL + username).then((res) => {
        if (res && res.status === 200) {
            let data = JSON.parse(res.responseText);

            if (data.error) {
                showToastMessage(this.$t("message.toast_user_info_get_fail"));
            } else {
                profile_info = data.info;

                let app = Vue.createApp({
                    components: {
                        "track-item": trackItemComponent
                    },
                    data() {
                        return {
                            uploads_count: profile_info.uploads.length,
                            favorites_count: profile_info.favorites.length,
                            uploads_list: [],
                            is_uploads_list_visible: false,
                            user_description: profile_info.description,
                            user_password: "",
                            is_editing: false,
                            is_editing_password: false,
                            relative_registered_on: moment(profile_info.registered_on).fromNow(),
                            relative_last_seen: profile_info.last_seen ? moment(profile_info.last_seen).fromNow() : null,
                            all_songs_info: {},
                        };
                    },
                    mounted() {
                        i18n.global.locale = HttpHandler.setLangFromCookie();

                        // get uploads data
                        getAllSongsInfo().then(data => {
                            this.all_songs_info = data;

                            this.uploads_list = this.all_songs_info.filter(i => profile_info.uploads.includes(i._id)).reverse();
                        });

                        // Render markdown
                        this.$refs["user-desc"].innerHTML = getMarkdownHTML(this.user_description);
                    },

                    methods: {
                        uploadNewAvatar(event) {
                            if (event.target.files[0].size > MAX_AVATAR_SIZE) {
                                showToastMessage(`Maximum file size is ${formatBytes(MAX_AVATAR_SIZE)}. Please choose a smaller image.`);
                                return;
                            }

                            showToastMessage(this.$t("message.profile_avatar_uploading"));

                            this.$refs["user-avatar"].submit();
                        },

                        uploadNewBackground(event) {
                            if (event.target.files[0].size > MAX_AVATAR_SIZE) {
                                showToastMessage(`Maximum file size is ${formatBytes(MAX_AVATAR_SIZE)}. Please choose a smaller image.`);
                                return;
                            }

                            showToastMessage(this.$t("message.profile_background_uploading"));

                            this.$refs["background-form"].submit();
                        },

                        removeCustomBackground() {
                            HttpHandler.putAsync(HttpHandler.USER_UPDATE_URL + profile_info.username, {
                                custom_background_url: null
                            }, (res) => {
                                res.json().then((data) => {
                                    if (data.error) {
                                        showToastMessage(this.$t("message.profile_background_remove_fail"));
                                    } else {
                                        showToastMessage(this.$t("message.profile_background_remove"));

                                        setTimeout(() => {
                                            window.location.reload(true);
                                        }, 500);
                                    }
                                });
                            });
                        },

                        toggleUploadsListDisplay() {
                            this.is_uploads_list_visible = !this.is_uploads_list_visible;
                        },

                        cancelUserPassword() {
                            this.is_editing_password = false;
                        },
                        changeUserPassword() {
                            this.is_editing_password = false;

                            HttpHandler.putAsync(HttpHandler.USER_UPDATE_PASSWORD_URL + profile_info.username, {
                                password: this.user_password
                            }, (res) => {
                                res.json().then((data) => {
                                    if (data.error) {
                                        showToastMessage(this.$t("message.profile_password_update_fail"));
                                    } else {
                                        showToastMessage(this.$t("message.profile_password_update"));
                                        this.user_password = "";
                                    }
                                });
                            });

                            return false;
                        },

                        cancelUserDescription() {
                            this.is_editing = false;
                            this.user_description = profile_info.description;
                        },
                        changeUserDescription() {
                            this.is_editing = false;

                            if (this.user_description === profile_info.description) return;

                            HttpHandler.putAsync(HttpHandler.USER_UPDATE_URL + profile_info.username, {
                                description: this.user_description
                            }, (res) => {
                                res.json().then((data) => {
                                    if (data.error) {
                                        showToastMessage(this.$t("message.profile_description_update_fail"));
                                    } else {
                                        showToastMessage(this.$t("message.profile_description_update"));
                                        // Render markdown
                                        this.$refs["user-desc"].innerHTML = getMarkdownHTML(this.user_description);
                                    }
                                });
                            });
                        }
                    }
                });
                app.use(i18n);
                app = app.mount("#app");
            }
        }
    }).catch(() => {
        setTimeout(() => {
            getProfileInfo();
        }, 3000);
    });
}

function showToastMessage(message) {
    toastList[0].show();
    toastElList[0].querySelector(".toast-body > p").innerText = message;
}
