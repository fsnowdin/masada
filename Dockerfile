FROM node:lts-alpine

WORKDIR /opt/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build-sass

ENV PORT=3000

EXPOSE 3000

CMD [ "npm", "start" ]
