"use strict";

import fs from "fs";
import multer from "multer";

import { extname } from "path";

export const BASE_LOCAL_PATH = process.env.STORAGE_PATH;
export const BASE_PUBLIC_PATH = process.env.STORAGE_PATH.substr(1);

const ACCEPTED_IMAGE_TYPES = [
  ".jpg",
  ".png",
  ".jpeg",
  ".webp",
  ".gif",
];

export const upload = multer({
  dest: "/tmp",
  fileFilter: (req, file, cb) => {
    if (ACCEPTED_IMAGE_TYPES.includes(extname(file.originalname))) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  },
  limits: {
    fileSize: process.env.MAX_AVATAR_SIZE
  }
});

export const fileHandler = {};

// Create a file. Fails if the filepath already exists.
fileHandler.create = (filepath, data, callback) => {
  fs.open(filepath, "wx", (err, fd) => {
    if (!err && fd) {
      fs.writeFile(fd, data, (err) => {
        if (!err) {
          fs.close(fd, (err) => {
            if (!err) {
              callback(false);
            } else {
              callback("Error closing new file");
            }
          });
        } else {
          callback("Error writing to new file");
        }
      });
    } else {
      callback(`${err}\nCould not create new file. It may already exists.`);
    }
  });
};

// Create a file synchronously. Fails if already exists.
fileHandler.createSync = (path, data) => {
  const fd = fs.openSync(path, "wx");
  if (!fd) throw Error("Couldn't synchronously open file.");
  fs.writeFileSync(fd, data);
  fs.closeSync(fd);
};

// Create a directory
fileHandler.mkDir = (path, callback) => {
  if (fileHandler.existsSync(path)) {
    callback(null);
    return;
  }
  fs.mkdir(path, callback);
};

// Read data from a file async.
fileHandler.read = (path, callback) => {
  fs.readFile(path, { encoding: "utf-8", flag: "r" }, (err, data) => {
    if (!err && data) callback(false, data);
    else callback(err, undefined);
  });
};
// Read data synchronously from a file
fileHandler.readSync = (path) => {
  try {
    return fs.readFileSync(path, "utf-8", "rx");
  } catch (err) {
    return undefined;
  }
};

fileHandler.readDir = (path, callback) => {
  fs.readdir(path, (err, files) => {
    callback(err, files);
  });
};

// Update (replace) an already existing file
fileHandler.update = (path, data, callback) => {
  fs.writeFile(path, data, { encoding: "utf-8", flag: "w" }, (err) => {
    if (err) callback(`${err}. Could not update file. It may not exists.`);
    else callback(false);
  });
};
fileHandler.updateSync = (path, data) => {
  fs.writeFileSync(path, data);
};

// Delete a file
fileHandler.delete = (path, callback) => {
  fs.unlink(path, (err) => {
    if (err) callback(`${err}. Could not delete file.`);
    else callback(false);
  });
};

// Delete a file synchronously
fileHandler.deleteSync = (path) => {
  fs.unlinkSync(path);
};

// Check if a file/folder exists with a given path
fileHandler.existsSync = (path) => {
  return fs.existsSync(path);
};

fileHandler.copy = (source, destination, callback) => {
  fs.copyFile(source, destination, (err) => {
    callback(err);
  });
};
