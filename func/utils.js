let _iso8601DurationRegex = /(-)?P(?:([.,\d]+)Y)?(?:([.,\d]+)M)?(?:([.,\d]+)W)?(?:([.,\d]+)D)?T(?:([.,\d]+)H)?(?:([.,\d]+)M)?(?:([.,\d]+)S)?/;

export function replaceUnderscoreWithSpace(text) {
	if (!text) return;
	text = text.replace(/_/g, " ");
	return text;
}

export function getRangedRandomIndex(range) {
	if(range <= 0) return 0;
	return Math.floor(Math.random() * range);
}

export function formatBytes(bytes, decimals = 0) {
    if (bytes === 0) return "0 Bytes";

    const k = 1000;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

export function parseYoutubeDuration(iso8601Duration) {
    let matches = iso8601Duration.match(_iso8601DurationRegex);

    let timemap = {
        // sign: matches[1] === undefined ? "+" : "-",
        // years: matches[2] === undefined ? 0 : matches[2],
        // months: matches[3] === undefined ? 0 : matches[3],
        // weeks: matches[4] === undefined ? 0 : matches[4],
        // days: matches[5] === undefined ? 0 : matches[5],
        hours: matches[6] === undefined ? 0 : matches[6],
        minutes: matches[7] === undefined ? 0 : matches[7],
        seconds: matches[8] === undefined ? 0 : matches[8]
    };

    return timemap.hours * 3600 + timemap.minutes * 60 + timemap.seconds;
}
