import { checkLogin } from "../routes/user.js";
import { UserRoles } from "../models/user.js";

export function authenticate(req, callback, callback_fail) {
    // -----------------------------------------------------------------------
    // authentication middleware

    try {
        // TODO: Actually do proper authentication

        // const auth = { login: process.env.AUTH_USERNAME, password: process.env.AUTH_PASSWD };

        // parse login and password from headers
        // const b64auth = (req.headers.authorization || "").split(" ")[1] || "";
        // const [login, password] = Buffer.from(b64auth, "base64").toString().split(":");

        // Verify login and password are set and correct
        // if (login && password && login === auth.login && password === auth.password) {
            // Access granted...
            callback();
        // } else {
        //     callback_fail();
        // }
    } catch(err) {
        console.error(`${err}\nFailed to authenticate for request: ${req}`);
        callback_fail(err);
    }
}

export function authenticateUser(req) {
    return new Promise((resolve) => {
        if (req.session.userInfo) resolve(req.session.userInfo);

        // Use basic HTTP authentication
        const b64auth = (req.headers.authorization || "").split(" ")[1] || "";
        const [login, password] = Buffer.from(b64auth, "base64").toString().split(":");

        // Verify login and password are set and correct
        if (login && password) {
            checkLogin(login, password).then(result => {
                if (result === false) resolve(result);

                // Only administrators can use basic auth for the API
                if (result.rank !== UserRoles.ADMIN) resolve(false);

                resolve(result);
            }).catch(() => {
                resolve(false);
            });
        } else {
            resolve(false);
        }
    });
}
