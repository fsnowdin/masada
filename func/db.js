import mongodb from "mongodb";

// URL for MongoDB
const uri = process.env.DB_URL;

const client = new mongodb.MongoClient(uri, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});


const UPDATE_OPERATORS = {
    "SET" : "$set"
};

let connection = new Promise((resolve, reject) => {
	client.connect((err, result) => {
		if (err) {
			console.error(err);
			reject(result);
			return;
		}

		connection = client.db(process.env.DB_NAME);

		console.log(connection ? "Connection to MongoDB database established..." : "ERROR: Failed to find database...");

		resolve(connection);
	});
});

function isMongoId(str) {
	return str.length === 24;
}

export { mongodb, connection, UPDATE_OPERATORS, isMongoId };
