import { BASE_PUBLIC_PATH } from "../func/file.js";

const UserRoles = Object.freeze({
    ADMIN: "admin",
    NORMAL: "normal",
});

const DEFAULT_AVATAR_URL = BASE_PUBLIC_PATH + "/avatars/default/";

class UserModel {
    _id;
    username;
    password;
    email;
    avatar_url;
    rank = UserRoles.NORMAL;
    uploads = [];
    registered_on;
    description = "Hi!";
    preferences = {
        randomize_bg: false,
        auto_join_chat: true
    };
    custom_background_url;
    favorites = [];

    constructor(id, username, password, email, avatar_url, rank, registered_on) {
        this._id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.avatar_url = avatar_url;
        this.rank = rank;
        this.registered_on = registered_on;
    }
}


class MicroUser {
    _id;
    username;
    avatar_url;

    constructor(id, username, avatar_url) {
        this._id = id;
        this.username = username;
        this.avatar_url = avatar_url;
    }
}

export { UserRoles, UserModel, DEFAULT_AVATAR_URL, MicroUser };
