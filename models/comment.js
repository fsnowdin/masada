export class CommentModel {
    _id;
    user;
    href;
    content;
    timestamp;

    constructor(id, user_info, content, timestamp) {
        this._id = id;
        this.user = user_info;
        this.content = content;
        this.timestamp = timestamp;
        this.href= "/user/" + this.user.username;
    }
}
