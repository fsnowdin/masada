import { UserModel } from "../models/user.js";

class TrackModel {
	_id;
	img_src;
	img_src_wide;
	comments=[];
	upload_time;
	name;
    duration;
	description;
	favorites = [];
	poster = new UserModel();

	constructor(path, name, description, upload_time, poster, img_src, img_src_wide, duration) {
		this.path = path;
		this.name = name;
		this.description = description;
		this.upload_time = upload_time;
		this.poster = poster;
		this.img_src = img_src;
		this.img_src_wide = img_src_wide ?? this.img_src;
    this.duration = duration;
	}
}

export { TrackModel };
